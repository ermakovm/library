package info.mermakov.dev.library.service;

import info.mermakov.dev.library.dto.BookRentDto;
import info.mermakov.dev.library.dto.ClientDto;
import info.mermakov.dev.library.model.Client;

import java.util.List;

public interface ClientService {

    List<ClientDto> findAllClients();

    Client getClientById(Long id);

    ClientDto findClientById(Long id);

    ClientDto findClientByEmail(String email);

    ClientDto findClientByPhone(String phone);

    List<ClientDto> findClientByName(String firstName, String middleName, String lastName);

    List<BookRentDto> findRentsByClientId(Long id);

    ClientDto save(ClientDto clientDto);

    void removeClientById(Long id);

    ClientDto convertToDto(Client client);

    List<ClientDto> convertToDto(Iterable<Client> clients);
}
