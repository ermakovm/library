package info.mermakov.dev.library.service.util;

public class ErrorMessage {
    public static final String NO_FOUND_ERROR = "Not found any elements";
    public static final String NO_FOUND_BY_ID_ERROR = "Not found element by id ";
    public static final String NO_FOUND_BY_NAME_ERROR = "Not found element by name ";
    public static final String NO_FOUND_BY_TITLE_ERROR = "Not found books by title ";
    public static final String NO_FOUND_BOOKS_BY_ID = "Not found book by element id ";
    public static final String NO_FOUND_BY_EMAIL_ERROR = "Not found by email ";
    public static final String NO_FOUND_BY_PHONE_ERROR = "Not found by phone ";
    public static final String NO_FOUND_RENTS_BY_ID_ERROR = "Not found rents by element id";
    public static final String NO_FOUND_NOT_RETURNED_ERROR = "Not found not returned rents";
    public static final String FIELD_INVALID_ERROR = "Invalid field ";
    public static final String ALREADY_EXIST_ERROR = "Element with specified name already exist";
    public static final String ASSOCIATED_BOOK_ERROR = "You must first delete or modify books associated with element";
    public static final String PHONE_ALREADY_EXIST_ERROR = "Phone already associated with client";
    public static final String EMAIL_ALREADY_EXIST_ERROR = "Email already associated with client";
    public static final String ISBN_ALREADY_EXIST_ERROR = "ISBN already associated with book";
    public static final String CLIENT_HAVE_RENT_ERROR = "Client has not returned books";
    public static final String BOOK_RENTED_ERROR = "Book rented now";
    public static final String BOOK_EXISTS_ERROR = "Book already exists";
    public static final String RENT_EXISTS_ERROR = "Rent already exists";
}
