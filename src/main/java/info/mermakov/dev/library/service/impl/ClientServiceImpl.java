package info.mermakov.dev.library.service.impl;

import info.mermakov.dev.library.dto.BookRentDto;
import info.mermakov.dev.library.dto.ClientDto;
import info.mermakov.dev.library.exception.InvalidRequestException;
import info.mermakov.dev.library.exception.ResourceNotFoundException;
import info.mermakov.dev.library.model.BookRent;
import info.mermakov.dev.library.model.Client;
import info.mermakov.dev.library.repository.ClientRepository;
import info.mermakov.dev.library.service.BookRentService;
import info.mermakov.dev.library.service.ClientService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.*;

import static info.mermakov.dev.library.service.util.ErrorMessage.*;

@Service
public class ClientServiceImpl implements ClientService {
    private final ClientRepository repository;
    private final BookRentService rentService;

    @Autowired
    public ClientServiceImpl(ClientRepository repository, @Lazy BookRentService rentService) {
        this.repository = repository;
        this.rentService = rentService;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<ClientDto> findAllClients() {
        List<Client> clients = repository.findAll();
        if (clients.size() > 0) {
            return convertToDto(clients);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    @Override
    public Client getClientById(Long id) {
        Optional<Client> client = repository.findById(id);
        if (client.isPresent()) {
            return client.get();
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_ID_ERROR + id.toString());
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public ClientDto findClientById(Long id) {
        return convertToDto(getClientById(id));
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public ClientDto findClientByEmail(String email) {
        if (email == null) {
            throw new InvalidRequestException(FIELD_INVALID_ERROR + "email");
        }
        Client client = getClientOrNullByEmail(email);
        if (client != null) {
            return convertToDto(client);
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_EMAIL_ERROR + email);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public ClientDto findClientByPhone(String phone) {
        if (phone == null) {
            throw new InvalidRequestException(FIELD_INVALID_ERROR + "phone");
        }
        Client client = getClientOrNullByPhone(phone);
        if (client != null) {
            return convertToDto(client);
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_PHONE_ERROR + "phone");
    }

    private Client getClientOrNullByEmail(String email) {
        Optional<Client> client = repository.findByEmail(email);
        return client.orElse(null);
    }

    private Client getClientOrNullByPhone(String phone) {
        Optional<Client> client = repository.findByPhone(phone);
        return client.orElse(null);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<ClientDto> findClientByName(String firstName, String middleName, String lastName) {
        Set<Client> result = new HashSet<>();
        if (firstName != null) {
            result.addAll(repository.findByFirstNameIsNotNullAndFirstNameContains(firstName));
        }
        if (middleName != null) {
            result.addAll(repository.findByMiddleNameIsNotNullAndMiddleNameContains(middleName));
        }
        if (lastName != null) {
            result.addAll(repository.findByLastNameIsNotNullAndLastNameContains(lastName));
        }
        if (result.size() > 0) {
            return convertToDto(result);
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_NAME_ERROR);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<BookRentDto> findRentsByClientId(Long id) {
        Client client = getClientById(id);
        Set<BookRent> rents = client.getRents();
        if (rents.size() > 0) {
            return rentService.convertToDto(rents);
        }
        throw new ResourceNotFoundException(NO_FOUND_RENTS_BY_ID_ERROR + id.toString());
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public ClientDto save(@Validated ClientDto clientDto) {
        Client client;
        if (clientDto.getId() != null) {
            client = getClientById(clientDto.getId());
        } else {
            client = new Client();
        }
        Client clientByEmail = getClientOrNullByEmail(clientDto.getEmail());
        if (clientByEmail != null && !clientByEmail.getId().equals(clientDto.getId())) {
            throw new InvalidRequestException(EMAIL_ALREADY_EXIST_ERROR);
        }
        Client clientByPhone = getClientOrNullByPhone(clientDto.getPhone());
        if (clientByPhone != null && !clientByPhone.getId().equals(clientDto.getId())) {
            throw new InvalidRequestException(PHONE_ALREADY_EXIST_ERROR);
        }
        BeanUtils.copyProperties(clientDto, client, "id");
        return convertToDto(repository.saveAndFlush(client));
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void removeClientById(Long id) {
        Client client = getClientById(id);
        Set<BookRent> rents = client.getRents();
        for (BookRent rent : rents) {
            if (rent.getReturnDate() == null) {
                throw new InvalidRequestException(CLIENT_HAVE_RENT_ERROR);
            }
        }
        for (BookRent rent : rents) {
            rentService.removeRentById(rent.getId());
        }
        repository.save(client);
        repository.delete(client);
    }

    @Override
    public ClientDto convertToDto(Client client) {
        ClientDto clientDto = new ClientDto();
        BeanUtils.copyProperties(client, clientDto);
        return clientDto;
    }

    @Override
    public List<ClientDto> convertToDto(Iterable<Client> clients) {
        List<ClientDto> result = new ArrayList<>();
        for (Client client : clients) {
            result.add(convertToDto(client));
        }
        return result;
    }
}