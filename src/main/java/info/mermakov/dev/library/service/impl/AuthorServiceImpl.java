package info.mermakov.dev.library.service.impl;

import info.mermakov.dev.library.dto.AuthorDto;
import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.exception.InvalidRequestException;
import info.mermakov.dev.library.exception.ResourceNotFoundException;
import info.mermakov.dev.library.model.Author;
import info.mermakov.dev.library.model.Book;
import info.mermakov.dev.library.repository.AuthorRepository;
import info.mermakov.dev.library.service.AuthorService;
import info.mermakov.dev.library.service.BookService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.*;

import static info.mermakov.dev.library.service.util.ErrorMessage.*;

@Service
public class AuthorServiceImpl implements AuthorService {
    private final AuthorRepository repository;
    private final BookService bookService;

    @Autowired
    public AuthorServiceImpl(AuthorRepository repository, @Lazy BookService bookService) {
        this.repository = repository;
        this.bookService = bookService;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<AuthorDto> findAllAuthors() {
        List<Author> authors = repository.findAll();
        if (authors.size() > 0) {
            return convertToDto(authors);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    private Author getAuthorById(Long id) {
        Optional<Author> author = repository.findById(id);
        if (author.isPresent()) {
            return author.get();
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_ID_ERROR + id.toString());
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public AuthorDto findAuthorById(Long id) {
        return convertToDto(getAuthorById(id));
    }

    @Override
    public Author getAuthorOrNullByName(String firstName, String middleName, String lastName) {
        Optional<Author> author = repository.findByFirstNameAndMiddleNameAndLastName(firstName, middleName, lastName);
        return author.orElse(null);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<AuthorDto> findAuthorsByName(String firstName, String middleName, String lastName) {
        Set<Author> authors = new HashSet<>();
        if (firstName != null) {
            authors.addAll(repository.findByFirstNameIsNotNullAndFirstNameContains(firstName));
        }
        if (middleName != null) {
            authors.addAll(repository.findByMiddleNameIsNotNullAndMiddleNameContains(middleName));
        }
        if (lastName != null) {
            authors.addAll(repository.findByLastNameIsNotNullAndLastNameContains(lastName));
        }
        if (authors.size() > 0) {
            return convertToDto(authors);
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_NAME_ERROR);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<BookDto> findBooksByAuthorId(Long id) {
        Author author = getAuthorById(id);
        Set<Book> books = author.getBooks();
        if (books.size() > 0) {
            return bookService.convertToDto(books);
        }
        throw new ResourceNotFoundException(NO_FOUND_BOOKS_BY_ID + id.toString());
    }

    @Override
    public Author saveAuthor(@Validated AuthorDto authorDto) {
        Author author;
        if (authorDto.getId() != null) {
            author = getAuthorById(authorDto.getId());
        } else {
            author = new Author();
        }
        Author authorByName = getAuthorOrNullByName(authorDto.getFirstName(),
                authorDto.getMiddleName(), authorDto.getLastName());
        if (authorByName != null && !authorByName.getId().equals(authorDto.getId())) {
            throw new InvalidRequestException(ALREADY_EXIST_ERROR);
        }
        BeanUtils.copyProperties(authorDto, author, "id");
        return repository.saveAndFlush(author);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public AuthorDto save(AuthorDto authorDto) {
        Author author = saveAuthor(authorDto);
        return convertToDto(author);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void removeAuthorById(Long id) {
        Author author = getAuthorById(id);
        Set<Book> books = author.getBooks();
        books.removeAll(books);
        repository.save(author);
        repository.deleteById(id);
    }

    @Override
    public AuthorDto convertToDto(Author author) {
        AuthorDto authorDto = new AuthorDto();
        BeanUtils.copyProperties(author, authorDto);
        return authorDto;
    }

    @Override
    public List<AuthorDto> convertToDto(Iterable<Author> authors) {
        List<AuthorDto> result = new ArrayList<>();
        for (Author author : authors) {
            result.add(convertToDto(author));
        }
        return result;
    }
}