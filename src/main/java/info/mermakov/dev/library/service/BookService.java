package info.mermakov.dev.library.service;

import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.dto.BookRentDto;
import info.mermakov.dev.library.model.Book;

import java.util.List;

public interface BookService {

    List<BookDto> findAllBooks();

    Book getBookById(Long id);

    BookDto findBookById(Long id);

    List<BookDto> findBooksByTitle(String title);

    BookDto save(BookDto bookDto);

    void removeBookById(Long id);

    List<BookRentDto> findRentByBookId(Long id);

    BookDto convertToDto(Book book);

    List<BookDto> convertToDto(Iterable<Book> books);
}
