package info.mermakov.dev.library.service.impl;

import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.dto.TypeDto;
import info.mermakov.dev.library.exception.InvalidRequestException;
import info.mermakov.dev.library.exception.ResourceNotFoundException;
import info.mermakov.dev.library.model.Book;
import info.mermakov.dev.library.model.Type;
import info.mermakov.dev.library.repository.TypeRepository;
import info.mermakov.dev.library.service.BookService;
import info.mermakov.dev.library.service.TypeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static info.mermakov.dev.library.service.util.ErrorMessage.*;

@Service
public class TypeServiceImpl implements TypeService {
    private final TypeRepository repository;
    private final BookService bookService;

    @Autowired
    public TypeServiceImpl(TypeRepository repository, @Lazy BookService bookService) {
        this.repository = repository;
        this.bookService = bookService;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<TypeDto> findAllTypes() {
        List<Type> types = repository.findAll();
        if (types.size() > 0) {
            return convertToDto(types);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    private Type getTypeById(Long id) {
        Optional<Type> type = repository.findById(id);
        if (type.isPresent()) {
            return type.get();
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_ID_ERROR + id.toString());
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public TypeDto findTypeById(Long id) {
        return convertToDto(getTypeById(id));
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public Type getTypeOrNullByName(String name) {
        Optional<Type> result = repository.findByName(name);
        return result.orElse(null);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<TypeDto> findTypesByName(String name) {
        if (name == null) {
            throw new InvalidRequestException(FIELD_INVALID_ERROR + "'name'");
        }
        List<Type> types = repository.findByNameContains(name);
        if (types.size() > 0) {
            return convertToDto(types);
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_NAME_ERROR + name);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<BookDto> findBooksByTypeId(Long id) {
        Type type = getTypeById(id);
        Set<Book> books = type.getBooks();
        if (books.size() > 0) {
            return bookService.convertToDto(books);
        }
        throw new ResourceNotFoundException(NO_FOUND_BOOKS_BY_ID + id.toString());
    }

    @Override
    public Type saveType(@Validated TypeDto typeDto) {
        Type type;
        if (typeDto.getId() != null) {
            type = getTypeById(typeDto.getId());
        } else {
            type = new Type();
        }
        Type typeByName = getTypeOrNullByName(typeDto.getName());
        if (typeByName != null && !typeByName.getId().equals(typeDto.getId())) {
            throw new InvalidRequestException(ALREADY_EXIST_ERROR);
        }
        BeanUtils.copyProperties(typeDto, type, "id");
        return repository.saveAndFlush(type);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public TypeDto save(TypeDto typeDto) {
        Type type = saveType(typeDto);
        return convertToDto(type);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void removeTypeById(Long id) {
        Type type = getTypeById(id);
        Set<Book> books = type.getBooks();
        if (books.size() > 0) {
            throw new InvalidRequestException(ASSOCIATED_BOOK_ERROR);
        }
        repository.deleteById(id);
    }

    @Override
    public TypeDto convertToDto(Type type) {
        TypeDto typeDto = new TypeDto();
        BeanUtils.copyProperties(type, typeDto);
        return typeDto;
    }

    @Override
    public List<TypeDto> convertToDto(Iterable<Type> types) {
        List<TypeDto> result = new ArrayList<>();
        for (Type type : types) {
            result.add(convertToDto(type));
        }
        return result;
    }
}