package info.mermakov.dev.library.service;

import info.mermakov.dev.library.dto.BookRentDto;
import info.mermakov.dev.library.dto.SimpleBookRentDto;
import info.mermakov.dev.library.model.BookRent;

import java.time.LocalDate;
import java.util.List;

public interface BookRentService {

    List<BookRentDto> findAllRents();

    BookRentDto findRentById(Long id);

    List<BookRentDto> findNotReturnedRents();

    List<BookRentDto> findExpiredRentsOrNull(LocalDate currentDate);

    BookRentDto save(SimpleBookRentDto bookRentDto);

    void removeRentById(Long id);

    BookRentDto convertToDto(BookRent bookRent);

    List<BookRentDto> convertToDto(Iterable<BookRent> bookRents);
}
