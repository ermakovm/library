package info.mermakov.dev.library.service.impl;

import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.dto.PublisherDto;
import info.mermakov.dev.library.exception.InvalidRequestException;
import info.mermakov.dev.library.exception.ResourceNotFoundException;
import info.mermakov.dev.library.model.Book;
import info.mermakov.dev.library.model.Publisher;
import info.mermakov.dev.library.repository.PublisherRepository;
import info.mermakov.dev.library.service.BookService;
import info.mermakov.dev.library.service.PublisherService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static info.mermakov.dev.library.service.util.ErrorMessage.*;

@Service
public class PublisherServiceImpl implements PublisherService {
    private final PublisherRepository repository;
    private final BookService bookService;

    @Autowired
    public PublisherServiceImpl(PublisherRepository repository, @Lazy BookService bookService) {
        this.repository = repository;
        this.bookService = bookService;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<PublisherDto> findAllPublishers() {
        List<Publisher> publishers = repository.findAll();
        if (publishers.size() > 0) {
            return convertToDto(publishers);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    private Publisher getPublisherById(Long id) {
        Optional<Publisher> publisher = repository.findById(id);
        if (publisher.isPresent()) {
            return publisher.get();
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_ID_ERROR + id.toString());
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public PublisherDto findPublisherById(Long id) {
        return convertToDto(getPublisherById(id));
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public Publisher getPublisherOrNullByName(String name) {
        Optional<Publisher> publisher = repository.findByName(name);
        return publisher.orElse(null);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<PublisherDto> findPublishersByName(String name) {
        if (name == null) {
            throw new InvalidRequestException(FIELD_INVALID_ERROR + "'name'");
        }
        List<Publisher> publishers = repository.findByNameContains(name);
        if (publishers.size() > 0) {
            return convertToDto(publishers);
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_NAME_ERROR + name);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<BookDto> findBooksByPublisherId(Long id) {
        Publisher publisher = getPublisherById(id);
        Set<Book> books = publisher.getBooks();
        if (books.size() > 0) {
            return bookService.convertToDto(books);
        }
        throw new ResourceNotFoundException(NO_FOUND_BOOKS_BY_ID + id.toString());
    }

    @Override
    public Publisher savePublisher(@Validated PublisherDto publisherDto) {
        Publisher publisher;
        if (publisherDto.getId() != null) {
            publisher = getPublisherById(publisherDto.getId());
        } else {
            publisher = new Publisher();
        }
        Publisher publisherByName = getPublisherOrNullByName(publisherDto.getName());
        if (publisherByName != null && !publisherByName.getId().equals(publisherDto.getId())) {
            throw new InvalidRequestException(ALREADY_EXIST_ERROR);
        }
        BeanUtils.copyProperties(publisherDto, publisher, "id");
        return repository.saveAndFlush(publisher);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public PublisherDto save(PublisherDto publisherDto) {
        Publisher publisher = savePublisher(publisherDto);
        return convertToDto(publisher);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void removePublisherById(Long id) {
        Publisher publisher = getPublisherById(id);
        Set<Book> books = publisher.getBooks();
        if (books.size() > 0) {
            throw new InvalidRequestException(ASSOCIATED_BOOK_ERROR);
        }
        repository.deleteById(id);
    }

    @Override
    public PublisherDto convertToDto(Publisher publisher) {
        PublisherDto publisherDto = new PublisherDto();
        BeanUtils.copyProperties(publisher, publisherDto);
        return publisherDto;
    }

    @Override
    public List<PublisherDto> convertToDto(Iterable<Publisher> publishers) {
        List<PublisherDto> result = new ArrayList<>();
        for (Publisher publisher : publishers) {
            result.add(convertToDto(publisher));
        }
        return result;
    }
}
