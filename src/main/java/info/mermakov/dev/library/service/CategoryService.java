package info.mermakov.dev.library.service;

import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.dto.CategoryDto;
import info.mermakov.dev.library.model.Category;

import java.util.List;

public interface CategoryService {

    List<CategoryDto> findAllCategories();

    CategoryDto findCategoryById(Long id);

    Category getCategoryOrNullByName(String name);

    List<CategoryDto> findCategoriesByName(String name);

    List<BookDto> findBooksByCategoryId(Long id);

    Category saveCategory(CategoryDto categoryDto);

    CategoryDto save(CategoryDto categoryDto);

    void removeCategoryById(Long id);

    CategoryDto convertToDto(Category category);

    List<CategoryDto> convertToDto(Iterable<Category> categories);
}
