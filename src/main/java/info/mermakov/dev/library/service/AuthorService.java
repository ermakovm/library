package info.mermakov.dev.library.service;

import info.mermakov.dev.library.dto.AuthorDto;
import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.model.Author;

import java.util.List;

public interface AuthorService {

    List<AuthorDto> findAllAuthors();

    AuthorDto findAuthorById(Long id);

    Author getAuthorOrNullByName(String firstName, String middleName, String lastName);

    List<AuthorDto> findAuthorsByName(String firstName, String middleName, String lastName);

    List<BookDto> findBooksByAuthorId(Long id);

    Author saveAuthor(AuthorDto authorDto);

    AuthorDto save(AuthorDto authorDto);

    void removeAuthorById(Long id);

    AuthorDto convertToDto(Author author);

    List<AuthorDto> convertToDto(Iterable<Author> authors);
}
