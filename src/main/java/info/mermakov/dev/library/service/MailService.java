package info.mermakov.dev.library.service;

import info.mermakov.dev.library.dto.BookDto;

public interface MailService {
    void send(String clientMail, BookDto book);
}
