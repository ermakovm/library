package info.mermakov.dev.library.service;

import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.dto.PublisherDto;
import info.mermakov.dev.library.model.Publisher;

import java.util.List;

public interface PublisherService {

    List<PublisherDto> findAllPublishers();

    PublisherDto findPublisherById(Long id);

    Publisher getPublisherOrNullByName(String name);

    List<PublisherDto> findPublishersByName(String name);

    List<BookDto> findBooksByPublisherId(Long id);

    Publisher savePublisher(PublisherDto publisherDto);

    PublisherDto save(PublisherDto publisherDto);

    void removePublisherById(Long id);

    PublisherDto convertToDto(Publisher publisher);

    List<PublisherDto> convertToDto(Iterable<Publisher> publishers);
}
