package info.mermakov.dev.library.service.impl;

import info.mermakov.dev.library.dto.*;
import info.mermakov.dev.library.exception.InvalidRequestException;
import info.mermakov.dev.library.exception.ResourceNotFoundException;
import info.mermakov.dev.library.model.*;
import info.mermakov.dev.library.repository.BookRepository;
import info.mermakov.dev.library.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.*;

import static info.mermakov.dev.library.service.util.ErrorMessage.*;

@Service
public class BookServiceImpl implements BookService {
    private final BookRepository repository;
    private final TypeService typeService;
    private final PublisherService publisherService;
    private final AuthorService authorService;
    private final CategoryService categoryService;
    private final BookRentService rentService;

    @Autowired
    public BookServiceImpl(BookRepository repository, TypeService typeService,
                           PublisherService publisherService, AuthorService authorService,
                           CategoryService categoryService, @Lazy BookRentService rentService) {
        this.repository = repository;
        this.typeService = typeService;
        this.publisherService = publisherService;
        this.authorService = authorService;
        this.categoryService = categoryService;
        this.rentService = rentService;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<BookDto> findAllBooks() {
        List<Book> books = repository.findAll();
        if (books.size() > 0) {
            return convertToDto(books);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    @Override
    public Book getBookById(Long id) {
        Optional<Book> book = repository.findById(id);
        if (book.isPresent()) {
            return book.get();
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_ID_ERROR + id.toString());
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public BookDto findBookById(Long id) {
        return convertToDto(getBookById(id));
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<BookDto> findBooksByTitle(String title) {
        if (title == null) {
            throw new InvalidRequestException(FIELD_INVALID_ERROR + "'title'");
        }
        List<Book> books = repository.findByTitleContains(title);
        if (books.size() > 0) {
            return convertToDto(books);
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_TITLE_ERROR + title);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<BookRentDto> findRentByBookId(Long id) {
        Book book = getBookById(id);
        Set<BookRent> rents = book.getRents();
        if (rents.size() > 0) {
            return rentService.convertToDto(rents);
        }
        throw new ResourceNotFoundException(NO_FOUND_RENTS_BY_ID_ERROR + id.toString());
    }

    private void updateCategories(Set<Category> categories, List<CategoryDto> categoriesDto) {
        Set<Category> newCategories = new HashSet<>();
        for (CategoryDto categoryDto : categoriesDto) {
            Category category = categoryService.getCategoryOrNullByName(categoryDto.getName());
            if (category == null) {
                categoryDto.setId(null);
                category = categoryService.saveCategory(categoryDto);
            }
            newCategories.add(category);
            categories.add(category);
        }
        categories.removeIf(category -> !newCategories.contains(category));
    }

    private void updateAuthors(Set<Author> authors, List<AuthorDto> authorsDto) {
        Set<Author> newAuthors = new HashSet<>();
        for (AuthorDto authorDto : authorsDto) {
            String firstName = authorDto.getFirstName();
            String middleName = authorDto.getMiddleName();
            String lastName = authorDto.getLastName();
            Author author = authorService.getAuthorOrNullByName(firstName, middleName, lastName);
            if (author == null) {
                authorDto.setId(null);
                author = authorService.saveAuthor(authorDto);
            }
            newAuthors.add(author);
            authors.add(author);
        }
        authors.removeIf(author -> !newAuthors.contains(author));
    }

    private Type getType(TypeDto typeDto) {
        Type type = typeService.getTypeOrNullByName(typeDto.getName());
        if (type == null) {
            typeDto.setId(null);
            return typeService.saveType(typeDto);
        }
        return type;
    }

    private Publisher getPublisher(PublisherDto publisherDto) {
        Publisher publisher = publisherService.getPublisherOrNullByName(publisherDto.getName());
        if (publisher == null) {
            publisherDto.setId(null);
            return publisherService.savePublisher(publisherDto);
        }
        return publisher;
    }

    private void setDependencies(Book book, BookDto bookDto) {
        TypeDto typeDto = bookDto.getType();
        book.setType(getType(typeDto));

        PublisherDto publisherDto = bookDto.getPublisher();
        book.setPublisher(getPublisher(publisherDto));

        List<AuthorDto> authorsDto = bookDto.getAuthors();
        Set<Author> authors = book.getAuthors();
        updateAuthors(authors, authorsDto);
        book.setAuthors(authors);

        List<CategoryDto> categoriesDto = bookDto.getCategories();
        Set<Category> categories = book.getCategories();
        updateCategories(categories, categoriesDto);
        book.setCategories(categories);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public BookDto save(@Validated BookDto bookDto) {
        Book book;
        if (bookDto.getId() != null) {
            book = getBookById(bookDto.getId());
        } else {
            book = new Book();
        }
        if (bookDto.getIsbn() != null) {
            Optional<Book> bookByIsbn = repository.findByIsbn(bookDto.getIsbn());
            if (bookByIsbn.isPresent()) {
                if (!bookByIsbn.get().getId().equals(bookDto.getId())) {
                    throw new InvalidRequestException(ISBN_ALREADY_EXIST_ERROR);
                }
            }
        }
        BeanUtils.copyProperties(bookDto, book, "id", "type", "publisher", "authors", "categories");
        setDependencies(book, bookDto);
        List<Book> books = repository.findByTitle(bookDto.getTitle());
        books.stream().filter(findBook -> findBook.like(book)).forEach(findBook -> {
            throw new InvalidRequestException(BOOK_EXISTS_ERROR);
        });
        return convertToDto(repository.saveAndFlush(book));
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void removeBookById(Long id) {
        Book book = getBookById(id);
        Set<Author> authors = book.getAuthors();
        Set<Category> categories = book.getCategories();
        Set<BookRent> rents = book.getRents();
        rents.stream().filter(rent -> rent.getReturnDate() == null).forEach(rent -> {
            throw new InvalidRequestException(BOOK_RENTED_ERROR);
        });
        rents.stream().map(BookRent::getId).forEach(rentService::removeRentById);
        categories.removeAll(categories);
        authors.removeAll(authors);
        repository.save(book);
        repository.deleteById(id);
    }

    @Override
    public BookDto convertToDto(Book book) {
        BookDto bookDto = new BookDto();
        BeanUtils.copyProperties(book, bookDto, "type", "publisher", "authors", "categories");

        Type type = book.getType();
        bookDto.setType(typeService.convertToDto(type));

        Publisher publisher = book.getPublisher();
        bookDto.setPublisher(publisherService.convertToDto(publisher));

        Set<Author> authors = book.getAuthors();
        bookDto.setAuthors(authorService.convertToDto(authors));

        Set<Category> categories = book.getCategories();
        bookDto.setCategories(categoryService.convertToDto(categories));

        return bookDto;
    }

    @Override
    public List<BookDto> convertToDto(Iterable<Book> books) {
        List<BookDto> result = new ArrayList<>();
        for (Book book : books) {
            result.add(convertToDto(book));
        }
        return result;
    }
}