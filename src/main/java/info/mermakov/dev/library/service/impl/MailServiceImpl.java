package info.mermakov.dev.library.service.impl;

import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@Profile("!test")
public class MailServiceImpl implements MailService {
    private final JavaMailSender mailSender;
    @Value("${spring.mail.username}")
    private String username;

    @Autowired
    public MailServiceImpl(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public void send(String clientMail, BookDto book) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(username);
        mailMessage.setTo(clientMail);
        mailMessage.setSubject("Book return");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Please return book:").append(book.getTitle()).append(", year: ").append(book.getRelease().toString());
        mailMessage.setText(stringBuilder.toString());
        mailSender.send(mailMessage);
    }
}
