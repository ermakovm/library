package info.mermakov.dev.library.service.impl;

import info.mermakov.dev.library.dto.BookRentDto;
import info.mermakov.dev.library.dto.SimpleBookRentDto;
import info.mermakov.dev.library.exception.InvalidRequestException;
import info.mermakov.dev.library.exception.ResourceNotFoundException;
import info.mermakov.dev.library.model.Book;
import info.mermakov.dev.library.model.BookRent;
import info.mermakov.dev.library.model.Client;
import info.mermakov.dev.library.repository.BookRentRepository;
import info.mermakov.dev.library.service.BookRentService;
import info.mermakov.dev.library.service.BookService;
import info.mermakov.dev.library.service.ClientService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static info.mermakov.dev.library.service.util.ErrorMessage.*;

@Service
public class BookRentServiceImpl implements BookRentService {
    private final BookRentRepository repository;
    private final BookService bookService;
    private final ClientService clientService;

    @Value("${days.for.return}")
    private int daysForReturn;

    @Autowired
    public BookRentServiceImpl(BookRentRepository repository, BookService bookService, ClientService clientService) {
        this.repository = repository;
        this.bookService = bookService;
        this.clientService = clientService;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<BookRentDto> findAllRents() {
        List<BookRent> rents = repository.findAll();
        if (rents.size() > 0) {
            return convertToDto(rents);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    private BookRent getRentById(Long id) {
        Optional<BookRent> rent = repository.findById(id);
        if (rent.isPresent()) {
            return rent.get();
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_ID_ERROR + id.toString());
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public BookRentDto findRentById(Long id) {
        return convertToDto(getRentById(id));
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<BookRentDto> findNotReturnedRents() {
        List<BookRent> rents = repository.findByReturnDateIsNull();
        if (rents.size() > 0) {
            return convertToDto(rents);
        }
        throw new ResourceNotFoundException(NO_FOUND_NOT_RETURNED_ERROR);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<BookRentDto> findExpiredRentsOrNull(LocalDate currentDate) {
        LocalDate date = currentDate.minusDays(daysForReturn);
        List<BookRent> rents = repository.findByReturnDateIsNullAndIssueDateBefore(date);
        if (rents.size() > 0) {
            return convertToDto(rents);
        }
        return null;
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public BookRentDto save(@Validated SimpleBookRentDto bookRentDto) {
        BookRent rent;
        if (bookRentDto.getId() != null) {
            rent = getRentById(bookRentDto.getId());
        } else {
            rent = new BookRent();
        }
        Client client = clientService.getClientById(bookRentDto.getClient_id());
        Book book = bookService.getBookById(bookRentDto.getBook_id());
        BeanUtils.copyProperties(bookRentDto, rent, "id", "book", "client");
        rent.setClient(client);
        rent.setBook(book);
        List<BookRent> rentsByClient = repository.findByClientId(client.getId());
        rentsByClient.stream().filter(bookRent -> bookRent.like(rent)).forEach(bookRent -> {
            throw new InvalidRequestException(RENT_EXISTS_ERROR);
        });
        return convertToDto(repository.saveAndFlush(rent));
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void removeRentById(Long id) {
        BookRent rent = getRentById(id);
        if (rent.getReturnDate() == null) {
            throw new InvalidRequestException(BOOK_RENTED_ERROR);
        }
        repository.deleteById(id);
    }

    @Override
    public BookRentDto convertToDto(BookRent bookRent) {
        BookRentDto rentDto = new BookRentDto();
        BeanUtils.copyProperties(bookRent, rentDto, "client", "book");
        Client client = bookRent.getClient();
        rentDto.setClient(clientService.convertToDto(client));
        Book book = bookRent.getBook();
        rentDto.setBook(bookService.convertToDto(book));
        return rentDto;
    }

    @Override
    public List<BookRentDto> convertToDto(Iterable<BookRent> bookRents) {
        List<BookRentDto> result = new ArrayList<>();
        for (BookRent rent : bookRents) {
            result.add(convertToDto(rent));
        }
        return result;
    }
}