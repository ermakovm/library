package info.mermakov.dev.library.service;

import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.dto.TypeDto;
import info.mermakov.dev.library.model.Type;

import java.util.List;

public interface TypeService {

    List<TypeDto> findAllTypes();

    TypeDto findTypeById(Long id);

    Type getTypeOrNullByName(String name);

    List<TypeDto> findTypesByName(String name);

    List<BookDto> findBooksByTypeId(Long id);

    Type saveType(TypeDto typeDto);

    TypeDto save(TypeDto typeDto);

    void removeTypeById(Long id);

    TypeDto convertToDto(Type type);

    List<TypeDto> convertToDto(Iterable<Type> types);
}
