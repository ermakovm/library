package info.mermakov.dev.library.service.impl;

import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.dto.CategoryDto;
import info.mermakov.dev.library.exception.InvalidRequestException;
import info.mermakov.dev.library.exception.ResourceNotFoundException;
import info.mermakov.dev.library.model.Book;
import info.mermakov.dev.library.model.Category;
import info.mermakov.dev.library.repository.CategoryRepository;
import info.mermakov.dev.library.service.BookService;
import info.mermakov.dev.library.service.CategoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static info.mermakov.dev.library.service.util.ErrorMessage.*;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository repository;
    private final BookService bookService;

    @Autowired
    public CategoryServiceImpl(CategoryRepository repository, @Lazy BookService bookService) {
        this.repository = repository;
        this.bookService = bookService;
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<CategoryDto> findAllCategories() {
        List<Category> categories = repository.findAll();
        if (categories.size() > 0) {
            return convertToDto(categories);
        }
        throw new ResourceNotFoundException(NO_FOUND_ERROR);
    }

    private Category getCategoryById(Long id) {
        Optional<Category> category = repository.findById(id);
        if (category.isPresent()) {
            return category.get();
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_ID_ERROR + id.toString());
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public CategoryDto findCategoryById(Long id) {
        return convertToDto(getCategoryById(id));
    }

    @Override
    public Category getCategoryOrNullByName(String name) {
        Optional<Category> category = repository.findByName(name);
        return category.orElse(null);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<CategoryDto> findCategoriesByName(String name) {
        if (name == null) {
            throw new InvalidRequestException(FIELD_INVALID_ERROR + "'name'");
        }
        List<Category> categories = repository.findByNameContains(name);
        if (categories.size() > 0) {
            return convertToDto(categories);
        }
        throw new ResourceNotFoundException(NO_FOUND_BY_NAME_ERROR + name);
    }

    @Override
    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    public List<BookDto> findBooksByCategoryId(Long id) {
        Category category = getCategoryById(id);
        Set<Book> books = category.getBooks();
        if (books.size() > 0) {
            return bookService.convertToDto(books);
        }
        throw new ResourceNotFoundException(NO_FOUND_BOOKS_BY_ID + id.toString());
    }

    @Override
    public Category saveCategory(@Validated CategoryDto categoryDto) {
        Category category;
        if (categoryDto.getId() != null) {
            category = getCategoryById(categoryDto.getId());
        } else {
            category = new Category();
        }
        Category categoryByName = getCategoryOrNullByName(categoryDto.getName());
        if (categoryByName != null && !categoryByName.getId().equals(categoryDto.getId())) {
            throw new InvalidRequestException(ALREADY_EXIST_ERROR);
        }
        BeanUtils.copyProperties(categoryDto, category, "id");
        return repository.saveAndFlush(category);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public CategoryDto save(CategoryDto categoryDto) {
        Category category = saveCategory(categoryDto);
        return convertToDto(category);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void removeCategoryById(Long id) {
        Category category = getCategoryById(id);
        Set<Book> books = category.getBooks();
        books.removeAll(books);
        repository.save(category);
        repository.deleteById(id);
    }

    @Override
    public CategoryDto convertToDto(Category category) {
        CategoryDto categoryDto = new CategoryDto();
        BeanUtils.copyProperties(category, categoryDto);
        return categoryDto;
    }

    @Override
    public List<CategoryDto> convertToDto(Iterable<Category> categories) {
        List<CategoryDto> result = new ArrayList<>();
        for (Category category : categories) {
            result.add(convertToDto(category));
        }
        return result;
    }
}