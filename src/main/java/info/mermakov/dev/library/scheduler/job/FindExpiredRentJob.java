package info.mermakov.dev.library.scheduler.job;

import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.dto.BookRentDto;
import info.mermakov.dev.library.service.BookRentService;
import info.mermakov.dev.library.service.MailService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
@Profile("!test")
public class FindExpiredRentJob implements Job {
    private final BookRentService rentService;
    private final MailService mailService;

    @Autowired
    public FindExpiredRentJob(BookRentService rentService, MailService mailService) {
        this.rentService = rentService;
        this.mailService = mailService;
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        LocalDate currentDate = LocalDate.now();
        try {
            List<BookRentDto> rents = rentService.findExpiredRentsOrNull(currentDate);
            if (rents != null) {
                for (BookRentDto rent : rents) {
                    BookDto book = rent.getBook();
                    String email = rent.getClient().getEmail();
                    mailService.send(email, book);
                }
            }
        } catch (Exception exception) {
            throw new JobExecutionException(exception);
        }
    }
}
