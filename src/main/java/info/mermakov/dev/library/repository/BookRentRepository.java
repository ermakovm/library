package info.mermakov.dev.library.repository;

import info.mermakov.dev.library.model.BookRent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface BookRentRepository extends JpaRepository<BookRent, Long> {
    List<BookRent> findByReturnDateIsNull();

    List<BookRent> findByReturnDateIsNullAndIssueDateBefore(LocalDate date);

    List<BookRent> findByClientId(Long id);
}
