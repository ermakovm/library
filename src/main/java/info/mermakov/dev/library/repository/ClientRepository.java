package info.mermakov.dev.library.repository;

import info.mermakov.dev.library.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {
    Optional<Client> findByEmail(String email);

    Optional<Client> findByPhone(String phone);

    List<Client> findByFirstNameIsNotNullAndFirstNameContains(String firstName);

    List<Client> findByMiddleNameIsNotNullAndMiddleNameContains(String middleName);

    List<Client> findByLastNameIsNotNullAndLastNameContains(String lastName);
}
