package info.mermakov.dev.library.repository;

import info.mermakov.dev.library.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AuthorRepository extends JpaRepository<Author, Long> {
    @org.springframework.data.jpa.repository.QueryHints({
            @javax.persistence.QueryHint(name = "org.hibernate.cacheable", value = "true")
    })
    Optional<Author> findByFirstNameAndMiddleNameAndLastName(String firstName, String middleName, String lastName);

    @org.springframework.data.jpa.repository.QueryHints({
            @javax.persistence.QueryHint(name = "org.hibernate.cacheable", value = "true")
    })
    List<Author> findByFirstNameIsNotNullAndFirstNameContains(String firstName);

    @org.springframework.data.jpa.repository.QueryHints({
            @javax.persistence.QueryHint(name = "org.hibernate.cacheable", value = "true")
    })
    List<Author> findByMiddleNameIsNotNullAndMiddleNameContains(String middleName);

    @org.springframework.data.jpa.repository.QueryHints({
            @javax.persistence.QueryHint(name = "org.hibernate.cacheable", value = "true")
    })
    List<Author> findByLastNameIsNotNullAndLastNameContains(String lastName);
}
