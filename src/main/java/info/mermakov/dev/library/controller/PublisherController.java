package info.mermakov.dev.library.controller;

import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.dto.PublisherDto;
import info.mermakov.dev.library.service.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/publishers")
public class PublisherController {
    private final PublisherService publisherService;

    @Autowired
    public PublisherController(PublisherService publisherService) {
        this.publisherService = publisherService;
    }

    @GetMapping
    public List<PublisherDto> findPublishers(@RequestBody(required = false) PublisherDto publisherDto) {
        if (publisherDto == null) {
            return publisherService.findAllPublishers();
        }
        return publisherService.findPublishersByName(publisherDto.getName());
    }

    @GetMapping("/{id}")
    public PublisherDto findPublisherById(@PathVariable Long id) {
        return publisherService.findPublisherById(id);
    }

    @GetMapping("/{id}/books")
    public List<BookDto> findBooksByPublisherId(@PathVariable Long id) {
        return publisherService.findBooksByPublisherId(id);
    }

    @PostMapping
    public PublisherDto savePublisher(@Validated @RequestBody PublisherDto publisherDto) {
        publisherDto.setId(null);
        return publisherService.save(publisherDto);
    }

    @PutMapping("/{id}")
    public PublisherDto savePublisher(@PathVariable Long id, @Validated @RequestBody PublisherDto publisherDto) {
        publisherDto.setId(id);
        return publisherService.save(publisherDto);
    }

    @DeleteMapping("/{id}")
    public void deletePublisher(@PathVariable Long id) {
        publisherService.removePublisherById(id);
    }
}
