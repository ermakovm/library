package info.mermakov.dev.library.controller;

import info.mermakov.dev.library.dto.BookRentDto;
import info.mermakov.dev.library.dto.SimpleBookRentDto;
import info.mermakov.dev.library.exception.ResourceNotFoundException;
import info.mermakov.dev.library.service.BookRentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(value = "/api/rents")
public class BookRentController {
    private final String NO_FOUND_EXPIRED_ERROR = "Not found expired rents";
    private final BookRentService rentService;

    @Autowired
    public BookRentController(BookRentService rentService) {
        this.rentService = rentService;
    }

    @GetMapping
    public List<BookRentDto> findAll() {
        return rentService.findAllRents();
    }

    @GetMapping("/{id}")
    public BookRentDto findById(@PathVariable Long id) {
        return rentService.findRentById(id);
    }

    @GetMapping("/noreturn")
    public List<BookRentDto> findNoReturnedRents() {
        return rentService.findNotReturnedRents();
    }

    @GetMapping("/expired")
    public List<BookRentDto> findExpiredRent() {
        LocalDate date = LocalDate.now();
        List<BookRentDto> rents = rentService.findExpiredRentsOrNull(date);
        if (rents != null) {
            return rents;
        }
        throw new ResourceNotFoundException(NO_FOUND_EXPIRED_ERROR);
    }

    @PostMapping
    public BookRentDto saveRent(@Validated @RequestBody SimpleBookRentDto simpleBookRentDto) {
        simpleBookRentDto.setId(null);
        return rentService.save(simpleBookRentDto);
    }

    @PutMapping("/{id}")
    public BookRentDto saveRent(@PathVariable Long id, @Validated @RequestBody SimpleBookRentDto simpleBookRentDto) {
        simpleBookRentDto.setId(id);
        return rentService.save(simpleBookRentDto);
    }

    @DeleteMapping("/{id}")
    public void deleteRent(@PathVariable Long id) {
        rentService.removeRentById(id);
    }
}