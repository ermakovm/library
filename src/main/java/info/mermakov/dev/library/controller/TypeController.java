package info.mermakov.dev.library.controller;

import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.dto.TypeDto;
import info.mermakov.dev.library.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/types")
public class TypeController {
    private final TypeService typeService;

    @Autowired
    public TypeController(TypeService typeService) {
        this.typeService = typeService;
    }

    @GetMapping
    public List<TypeDto> findTypes(@RequestBody(required = false) TypeDto typeDto) {
        if (typeDto == null) {
            return typeService.findAllTypes();
        }
        return typeService.findTypesByName(typeDto.getName());
    }

    @GetMapping("/{id}")
    public TypeDto findTypeById(@PathVariable Long id) {
        return typeService.findTypeById(id);
    }

    @GetMapping("/{id}/books")
    public List<BookDto> getBooksByTypeId(@PathVariable Long id) {
        return typeService.findBooksByTypeId(id);
    }

    @PostMapping
    public TypeDto saveType(@Validated @RequestBody TypeDto typeDto) {
        typeDto.setId(null);
        return typeService.save(typeDto);
    }

    @PutMapping("/{id}")
    public TypeDto saveType(@PathVariable Long id, @Validated @RequestBody TypeDto typeDto) {
        typeDto.setId(id);
        return typeService.save(typeDto);
    }

    @DeleteMapping("/{id}")
    public void deleteType(@PathVariable Long id) {
        typeService.removeTypeById(id);
    }
}
