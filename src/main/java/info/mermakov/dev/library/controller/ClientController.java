package info.mermakov.dev.library.controller;

import info.mermakov.dev.library.dto.BookRentDto;
import info.mermakov.dev.library.dto.ClientDto;
import info.mermakov.dev.library.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/clients")
public class ClientController {
    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping
    public List<ClientDto> findClients(@RequestBody(required = false) ClientDto clientDto) {
        if (clientDto == null) {
            return clientService.findAllClients();
        }
        String firstName = clientDto.getFirstName();
        String middleName = clientDto.getMiddleName();
        String lastName = clientDto.getLastName();
        return clientService.findClientByName(firstName, middleName, lastName);

    }

    @GetMapping("/email")
    public ClientDto findClientByEmail(@RequestBody ClientDto clientDto) {
        return clientService.findClientByEmail(clientDto.getEmail());
    }

    @GetMapping("/phone")
    public ClientDto findClientByPhone(@RequestBody ClientDto clientDto) {
        return clientService.findClientByPhone(clientDto.getPhone());
    }

    @GetMapping("/{id}")
    public ClientDto findClientById(@PathVariable Long id) {
        return clientService.findClientById(id);
    }

    @GetMapping("/{id}/rents")
    public List<BookRentDto> findRentsByClientId(@PathVariable Long id) {
        return clientService.findRentsByClientId(id);
    }

    @PostMapping
    public ClientDto saveClient(@Validated @RequestBody ClientDto clientDto) {
        clientDto.setId(null);
        return clientService.save(clientDto);
    }

    @PutMapping("/{id}")
    public ClientDto saveClient(@PathVariable Long id, @Validated @RequestBody ClientDto clientDto) {
        clientDto.setId(id);
        return clientService.save(clientDto);
    }

    @DeleteMapping("/{id}")
    public void deleteClient(@PathVariable Long id) {
        clientService.removeClientById(id);
    }
}
