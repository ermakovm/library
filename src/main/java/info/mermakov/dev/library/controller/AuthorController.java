package info.mermakov.dev.library.controller;

import info.mermakov.dev.library.dto.AuthorDto;
import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/authors")
public class AuthorController {
    private final AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping
    public List<AuthorDto> findAuthors(@RequestBody(required = false) AuthorDto authorDto) {
        if (authorDto == null) {
            return authorService.findAllAuthors();
        }
        String firstName = authorDto.getFirstName();
        String middleName = authorDto.getMiddleName();
        String lastName = authorDto.getLastName();
        return authorService.findAuthorsByName(firstName, middleName, lastName);
    }

    @GetMapping("/{id}")
    public AuthorDto findAuthorById(@PathVariable Long id) {
        return authorService.findAuthorById(id);
    }

    @GetMapping("/{id}/books")
    public List<BookDto> findBooksByAuthorId(@PathVariable Long id) {
        return authorService.findBooksByAuthorId(id);
    }

    @PostMapping
    public AuthorDto saveAuthor(@Validated @RequestBody AuthorDto authorDto) {
        authorDto.setId(null);
        return authorService.save(authorDto);
    }

    @PutMapping("/{id}")
    public AuthorDto saveAuthor(@PathVariable Long id, @Validated @RequestBody AuthorDto authorDto) {
        authorDto.setId(id);
        return authorService.save(authorDto);
    }

    @DeleteMapping("/{id}")
    public void deleteAuthor(@PathVariable Long id) {
        authorService.removeAuthorById(id);
    }
}