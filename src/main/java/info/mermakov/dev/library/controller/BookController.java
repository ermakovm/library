package info.mermakov.dev.library.controller;

import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.dto.BookRentDto;
import info.mermakov.dev.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/books")
public class BookController {
    private final BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    public List<BookDto> findBooks(@RequestBody(required = false) BookDto bookDto) {
        if (bookDto == null) {
            return bookService.findAllBooks();
        }
        return bookService.findBooksByTitle(bookDto.getTitle());
    }

    @GetMapping("/{id}")
    public BookDto findBookById(@PathVariable Long id) {
        return bookService.findBookById(id);
    }

    @GetMapping("/{id}/rents")
    public List<BookRentDto> findRentsByBookId(@PathVariable Long id) {
        return bookService.findRentByBookId(id);
    }

    @PostMapping
    public BookDto saveBook(@Validated @RequestBody BookDto bookDto) {
        bookDto.setId(null);
        return bookService.save(bookDto);
    }

    @PutMapping("/{id}")
    public BookDto saveBook(@PathVariable Long id, @Validated @RequestBody BookDto bookDto) {
        bookDto.setId(id);
        return bookService.save(bookDto);
    }

    @DeleteMapping("/{id}")
    public void deleteBook(@PathVariable Long id) {
        bookService.removeBookById(id);
    }
}
