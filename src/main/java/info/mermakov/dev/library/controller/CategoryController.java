package info.mermakov.dev.library.controller;

import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.dto.CategoryDto;
import info.mermakov.dev.library.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/categories")
public class CategoryController {
    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping
    public List<CategoryDto> findCategories(@RequestBody(required = false) CategoryDto categoryDto) {
        if (categoryDto == null) {
            return categoryService.findAllCategories();
        }
        return categoryService.findCategoriesByName(categoryDto.getName());
    }

    @GetMapping("/{id}")
    public CategoryDto findCategoryById(@PathVariable Long id) {
        return categoryService.findCategoryById(id);
    }

    @GetMapping("/{id}/books")
    public List<BookDto> findBooksByCategoryId(@PathVariable Long id) {
        return categoryService.findBooksByCategoryId(id);
    }

    @PostMapping
    public CategoryDto saveCategory(@Validated @RequestBody CategoryDto categoryDto) {
        categoryDto.setId(null);
        return categoryService.save(categoryDto);
    }

    @PutMapping("/{id}")
    public CategoryDto saveCategory(@PathVariable Long id, @Validated @RequestBody CategoryDto categoryDto) {
        categoryDto.setId(id);
        return categoryService.save(categoryDto);
    }

    @DeleteMapping("/{id}")
    public void deleteCategory(@PathVariable Long id) {
        categoryService.removeCategoryById(id);
    }
}
