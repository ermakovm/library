package info.mermakov.dev.library.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static info.mermakov.dev.library.model.util.ModelUtil.*;

@Entity
@Table(name = "client")
@Getter
@Setter
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name", nullable = false, length = NAME_MAX_LENGTH)
    @Size(min = NAME_MIN_LENGTH, max = NAME_MAX_LENGTH)
    private String firstName;

    @Column(name = "middle_name", length = NAME_MAX_LENGTH)
    @Size(min = NAME_MIN_LENGTH, max = NAME_MAX_LENGTH)
    private String middleName;

    @Column(name = "last_name", nullable = false, length = NAME_MAX_LENGTH)
    @Size(min = NAME_MIN_LENGTH, max = NAME_MAX_LENGTH)
    private String lastName;

    @Column(name = "email", nullable = false, unique = true, length = NAME_MAX_LENGTH)
    @Email
    private String email;

    @Column(name = "phone", nullable = false, unique = true, length = NAME_MAX_LENGTH)
    private String phone;

    @Column(name = "address", nullable = false, length = ADDRESS_MAX_LENGTH)
    @Size(min = ADDRESS_MIN_LENGTH, max = ADDRESS_MAX_LENGTH)
    private String address;

    @OneToMany(
            mappedBy = "client",
            cascade = CascadeType.REMOVE,
            fetch = FetchType.LAZY
    )
    private Set<BookRent> rents;

    public Client() {
        this.rents = new HashSet<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        if (!Objects.equals(id, client.id)) return false;
        if (!firstName.equals(client.firstName)) return false;
        if (!Objects.equals(middleName, client.middleName)) return false;
        if (!lastName.equals(client.lastName)) return false;
        if (!email.equals(client.email)) return false;
        if (!phone.equals(client.phone)) return false;
        return address.equals(client.address);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + firstName.hashCode();
        result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
        result = 31 * result + lastName.hashCode();
        result = 31 * result + email.hashCode();
        result = 31 * result + phone.hashCode();
        result = 31 * result + address.hashCode();
        return result;
    }
}
