package info.mermakov.dev.library.model.util;

public class ModelUtil {
    public static final int NAME_MIN_LENGTH = 1;
    public static final int NAME_MAX_LENGTH = 100;
    public static final int ADDRESS_MIN_LENGTH = 1;
    public static final int ADDRESS_MAX_LENGTH = 500;
    public static final int TITLE_MIN_LENGTH = 1;
    public static final int TITLE_MAX_LENGTH = 100;
}
