package info.mermakov.dev.library.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static info.mermakov.dev.library.model.util.ModelUtil.*;

@Entity
@Table(name = "publisher")
@Getter
@Setter
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Publisher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, unique = true, length = NAME_MAX_LENGTH)
    @Size(min = NAME_MIN_LENGTH, max = NAME_MAX_LENGTH)
    private String name;

    @Column(name = "address", nullable = false, length = ADDRESS_MAX_LENGTH)
    @Size(min = ADDRESS_MIN_LENGTH, max = ADDRESS_MAX_LENGTH)
    private String address;

    @OneToMany(
            mappedBy = "publisher",
            cascade = CascadeType.REMOVE,
            fetch = FetchType.LAZY
    )
    private Set<Book> books;

    public Publisher() {
        this.books = new HashSet<>();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Publisher publisher = (Publisher) o;

        if (!Objects.equals(id, publisher.id)) return false;
        if (!name.equals(publisher.name)) return false;
        return address.equals(publisher.address);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + name.hashCode();
        result = 31 * result + address.hashCode();
        return result;
    }
}
