package info.mermakov.dev.library.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.ISBN;

import javax.persistence.*;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.Year;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static info.mermakov.dev.library.model.util.ModelUtil.TITLE_MAX_LENGTH;
import static info.mermakov.dev.library.model.util.ModelUtil.TITLE_MIN_LENGTH;

@Entity
@Table(name = "book")
@Getter
@Setter
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", nullable = false, length = TITLE_MAX_LENGTH)
    @Size(min = TITLE_MIN_LENGTH, max = TITLE_MAX_LENGTH)
    private String title;

    @Column(name = "isbn")
    @ISBN
    private String isbn;

    @Column(name = "release_year", nullable = false)
    private Year release;

    @Column(name = "edition")
    @Positive
    private Integer edition;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "type_id", nullable = false)
    private Type type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "publisher_id", nullable = false)
    private Publisher publisher;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "book_author",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id")
    )
    private Set<Author> authors;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "book_category",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id")
    )
    private Set<Category> categories;

    @OneToMany(
            mappedBy = "book",
            cascade = CascadeType.REMOVE,
            fetch = FetchType.LAZY
    )
    private Set<BookRent> rents;

    public Book() {
        this.authors = new HashSet<>();
        this.categories = new HashSet<>();
        this.rents = new HashSet<>();
    }

    public boolean like(Book book) {
        if (!title.equals(book.title)) return false;
        if (!release.equals(book.release)) return false;
        if (!Objects.equals(edition, book.edition)) return false;
        if (!type.equals(book.type)) return false;
        if (!publisher.equals(book.publisher)) return false;
        if (authors.size() != book.authors.size()) return false;
        if (!authors.containsAll(book.authors)) return false;
        if (categories.size() != book.categories.size()) return false;
        return categories.containsAll(book.categories);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (!Objects.equals(id, book.id)) return false;
        if (!Objects.equals(isbn, book.isbn)) return false;
        return like(book);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + title.hashCode();
        result = 31 * result + (isbn != null ? isbn.hashCode() : 0);
        result = 31 * result + release.hashCode();
        result = 31 * result + (edition != null ? edition.hashCode() : 0);
        result = 31 * result + type.hashCode();
        result = 31 * result + publisher.hashCode();
        result = 31 * result + (authors != null ? authors.hashCode() : 0);
        result = 31 * result + (categories != null ? categories.hashCode() : 0);
        return result;
    }
}