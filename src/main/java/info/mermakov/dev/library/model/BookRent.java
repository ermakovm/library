package info.mermakov.dev.library.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "book_rent")
@NoArgsConstructor
@Getter
@Setter
public class BookRent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id", nullable = false)
    private Client client;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "book_id", nullable = false)
    private Book book;

    @Column(name = "issue_date", nullable = false)
    private LocalDate issueDate;

    @Column(name = "return_date")
    private LocalDate returnDate;

    public boolean like(BookRent rent) {
        if (!client.equals(rent.client)) return false;
        if (!book.equals(rent.book)) return false;
        if (!issueDate.equals(rent.issueDate)) return false;
        return Objects.equals(returnDate, rent.returnDate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BookRent rent = (BookRent) o;

        if (!Objects.equals(id, rent.id)) return false;
        return like(rent);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + client.hashCode();
        result = 31 * result + book.hashCode();
        result = 31 * result + issueDate.hashCode();
        result = 31 * result + (returnDate != null ? returnDate.hashCode() : 0);
        return result;
    }
}
