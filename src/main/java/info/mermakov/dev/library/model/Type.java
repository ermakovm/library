package info.mermakov.dev.library.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static info.mermakov.dev.library.model.util.ModelUtil.NAME_MAX_LENGTH;
import static info.mermakov.dev.library.model.util.ModelUtil.NAME_MIN_LENGTH;

@Entity
@Table(name = "type")
@Getter
@Setter
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Type {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, unique = true, length = NAME_MAX_LENGTH)
    @Size(min = NAME_MIN_LENGTH, max = NAME_MAX_LENGTH)
    private String name;

    @OneToMany(
            mappedBy = "type",
            cascade = CascadeType.REMOVE,
            fetch = FetchType.LAZY
    )
    private Set<Book> books;

    public Type() {
        this.books = new HashSet<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Type type = (Type) o;

        if (!Objects.equals(id, type.id)) return false;
        return name.equals(type.name);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + name.hashCode();
        return result;
    }
}
