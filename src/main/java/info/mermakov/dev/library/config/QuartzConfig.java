package info.mermakov.dev.library.config;

import info.mermakov.dev.library.scheduler.job.FindExpiredRentJob;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class QuartzConfig {
    @Value("${quartz.config.string}")
    private String config;

    @Bean
    @Profile("!test")
    public JobDetail getJobDetail() {
        return JobBuilder.newJob(FindExpiredRentJob.class)
                .withIdentity(FindExpiredRentJob.class.getSimpleName() + "-JobDetail-Identity")
                .storeDurably().build();
    }

    @Bean
    @Profile("!test")
    public Trigger getJobTrigger(JobDetail jobDetail) {
        return TriggerBuilder.newTrigger().forJob(jobDetail)
                .withIdentity(FindExpiredRentJob.class.getSimpleName() + "-Trigger-Identity")
                .withSchedule(CronScheduleBuilder.cronSchedule(config)).build();
    }
}