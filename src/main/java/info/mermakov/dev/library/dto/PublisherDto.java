package info.mermakov.dev.library.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.io.Serializable;

import static info.mermakov.dev.library.model.util.ModelUtil.*;

@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class PublisherDto implements Serializable {
    @Positive
    private Long id;

    @NotBlank
    @Size(min = NAME_MIN_LENGTH, max = NAME_MAX_LENGTH)
    private String name;

    @NotBlank
    @Size(min = ADDRESS_MIN_LENGTH, max = ADDRESS_MAX_LENGTH)
    private String address;
}
