package info.mermakov.dev.library.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class SimpleBookRentDto {
    @Positive
    private Long id;

    @NotNull
    @Positive
    private Long client_id;

    @NotNull
    @Positive
    private Long book_id;

    @NotNull
    @PastOrPresent
    private LocalDate issueDate;

    @PastOrPresent
    private LocalDate returnDate;
}
