package info.mermakov.dev.library.dto;

import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class BookRentDto {
    @Positive
    private Long id;

    @NotNull
    @Valid
    private ClientDto client;

    @NotNull
    @Valid
    private BookDto book;

    @NotNull
    @PastOrPresent
    private LocalDate issueDate;

    @PastOrPresent
    private LocalDate returnDate;
}
