package info.mermakov.dev.library.dto;

import lombok.*;
import org.hibernate.validator.constraints.ISBN;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Year;
import java.util.List;
import java.util.Objects;

import static info.mermakov.dev.library.model.util.ModelUtil.TITLE_MAX_LENGTH;
import static info.mermakov.dev.library.model.util.ModelUtil.TITLE_MIN_LENGTH;

@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class BookDto implements Serializable {
    @Positive
    private Long id;

    @NotBlank
    @Size(min = TITLE_MIN_LENGTH, max = TITLE_MAX_LENGTH)
    private String title;

    @ISBN
    private String isbn;

    @Positive
    private Integer edition;

    @Valid
    List<AuthorDto> authors;

    @NotNull
    @Valid
    private TypeDto type;

    @NotNull
    @Valid
    private PublisherDto publisher;
    @PastOrPresent
    @NotNull
    private Year release;

    @Valid
    List<CategoryDto> categories;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BookDto book = (BookDto) o;

        if (!Objects.equals(id, book.id)) return false;
        if (!title.equals(book.title)) return false;
        if (!Objects.equals(isbn, book.isbn)) return false;
        if (!release.equals(book.release)) return false;
        if (!Objects.equals(edition, book.edition)) return false;
        if (!type.equals(book.type)) return false;
        if (!publisher.equals(book.publisher)) return false;
        if (authors.size() != book.authors.size()) return false;
        if (!authors.containsAll(book.authors)) return false;
        if (categories.size() != book.categories.size()) return false;
        return categories.containsAll(book.categories);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + title.hashCode();
        result = 31 * result + (isbn != null ? isbn.hashCode() : 0);
        result = 31 * result + release.hashCode();
        result = 31 * result + (edition != null ? edition.hashCode() : 0);
        result = 31 * result + type.hashCode();
        result = 31 * result + publisher.hashCode();
        result = 31 * result + (authors != null ? authors.hashCode() : 0);
        result = 31 * result + (categories != null ? categories.hashCode() : 0);
        return result;
    }
}