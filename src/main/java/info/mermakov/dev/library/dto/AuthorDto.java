package info.mermakov.dev.library.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.io.Serializable;

import static info.mermakov.dev.library.model.util.ModelUtil.NAME_MAX_LENGTH;
import static info.mermakov.dev.library.model.util.ModelUtil.NAME_MIN_LENGTH;

@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class AuthorDto implements Serializable {
    @Positive
    private Long id;

    @NotBlank
    @Size(min = NAME_MIN_LENGTH, max = NAME_MAX_LENGTH)
    private String firstName;

    @Size(min = NAME_MIN_LENGTH, max = NAME_MAX_LENGTH)
    private String middleName;

    @NotBlank
    @Size(min = NAME_MIN_LENGTH, max = NAME_MAX_LENGTH)
    private String lastName;
}
