package info.mermakov.dev.library.service.author;

import info.mermakov.dev.library.dto.AuthorDto;
import info.mermakov.dev.library.model.Author;

import java.util.HashSet;

public class AuthorServiceTestUtil {
    public static final Author AUTHOR_1;
    public static final Author AUTHOR_2;
    public static final AuthorDto AUTHOR_DTO_1;
    public static final AuthorDto AUTHOR_DTO_2;

    static {
        AUTHOR_1 = new Author();
        AUTHOR_2 = new Author();
        AUTHOR_DTO_1 = new AuthorDto();
        AUTHOR_DTO_2 = new AuthorDto();
        initData();
    }

    public static void initData() {
        AUTHOR_1.setId(1L);
        AUTHOR_1.setFirstName("Ivan");
        AUTHOR_1.setMiddleName("Ivanovich");
        AUTHOR_1.setLastName("Ivanov");
        AUTHOR_1.setBooks(new HashSet<>());

        AUTHOR_2.setId(2L);
        AUTHOR_2.setFirstName("Petr");
        AUTHOR_2.setLastName("Petrov");
        AUTHOR_2.setBooks(new HashSet<>());

        AUTHOR_DTO_1.setId(1L);
        AUTHOR_DTO_1.setFirstName("Ivan");
        AUTHOR_DTO_1.setMiddleName("Ivanovich");
        AUTHOR_DTO_1.setLastName("Ivanov");

        AUTHOR_DTO_2.setId(2L);
        AUTHOR_DTO_2.setFirstName("Petr");
        AUTHOR_DTO_2.setLastName("Petrov");
    }
}
