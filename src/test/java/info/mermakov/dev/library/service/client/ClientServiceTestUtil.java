package info.mermakov.dev.library.service.client;

import info.mermakov.dev.library.dto.ClientDto;
import info.mermakov.dev.library.model.Client;

public class ClientServiceTestUtil {
    public static final Client CLIENT_1;
    public static final Client CLIENT_2;
    public static final ClientDto CLIENT_DTO_1;
    public static final ClientDto CLIENT_DTO_2;

    static {
        CLIENT_1 = new Client();
        CLIENT_2 = new Client();
        CLIENT_DTO_1 = new ClientDto();
        CLIENT_DTO_2 = new ClientDto();
        initData();
    }

    public static void initData() {
        CLIENT_1.setId(1L);
        CLIENT_1.setFirstName("Ivan");
        CLIENT_1.setMiddleName("Ivanovich");
        CLIENT_1.setLastName("Ivanov");
        CLIENT_1.setEmail("test@test.test");
        CLIENT_1.setPhone("+71231231231");
        CLIENT_1.setAddress("Address 1");

        CLIENT_2.setId(2L);
        CLIENT_2.setFirstName("Petr");
        CLIENT_2.setLastName("Petrov");
        CLIENT_2.setEmail("test2@test.test");
        CLIENT_2.setPhone("+7123123");
        CLIENT_2.setAddress("Address 1");

        CLIENT_DTO_1.setId(1L);
        CLIENT_DTO_1.setFirstName("Ivan");
        CLIENT_DTO_1.setMiddleName("Ivanovich");
        CLIENT_DTO_1.setLastName("Ivanov");
        CLIENT_DTO_1.setEmail("test@test.test");
        CLIENT_DTO_1.setPhone("+71231231231");
        CLIENT_DTO_1.setAddress("Address 1");

        CLIENT_DTO_2.setId(2L);
        CLIENT_DTO_2.setFirstName("Petr");
        CLIENT_DTO_2.setLastName("Petrov");
        CLIENT_DTO_2.setEmail("test2@test.test");
        CLIENT_DTO_2.setPhone("+7123123");
        CLIENT_DTO_2.setAddress("Address 1");
    }
}
