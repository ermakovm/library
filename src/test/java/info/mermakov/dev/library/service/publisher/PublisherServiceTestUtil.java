package info.mermakov.dev.library.service.publisher;

import info.mermakov.dev.library.dto.PublisherDto;
import info.mermakov.dev.library.model.Publisher;

import java.util.HashSet;

public class PublisherServiceTestUtil {
    public static final Publisher PUBLISHER_1;
    public static final Publisher PUBLISHER_2;
    public static final PublisherDto PUBLISHER_DTO_1;
    public static final PublisherDto PUBLISHER_DTO_2;

    static {
        PUBLISHER_1 = new Publisher();
        PUBLISHER_2 = new Publisher();

        PUBLISHER_DTO_1 = new PublisherDto();
        PUBLISHER_DTO_2 = new PublisherDto();
        initData();
    }

    public static void initData() {
        PUBLISHER_1.setId(1L);
        PUBLISHER_1.setName("Publisher 1");
        PUBLISHER_1.setAddress("Address 1");
        PUBLISHER_1.setBooks(new HashSet<>());

        PUBLISHER_2.setId(2L);
        PUBLISHER_2.setName("Publisher 2");
        PUBLISHER_2.setAddress("Address 2");
        PUBLISHER_2.setBooks(new HashSet<>());

        PUBLISHER_DTO_1.setId(1L);
        PUBLISHER_DTO_1.setName("Publisher 1");
        PUBLISHER_DTO_1.setAddress("Address 1");

        PUBLISHER_DTO_2.setId(2L);
        PUBLISHER_DTO_2.setName("Publisher 2");
        PUBLISHER_DTO_2.setAddress("Address 2");
    }
}