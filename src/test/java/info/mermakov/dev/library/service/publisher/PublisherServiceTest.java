package info.mermakov.dev.library.service.publisher;

import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.dto.PublisherDto;
import info.mermakov.dev.library.exception.InvalidRequestException;
import info.mermakov.dev.library.exception.ResourceNotFoundException;
import info.mermakov.dev.library.model.Book;
import info.mermakov.dev.library.model.Publisher;
import info.mermakov.dev.library.repository.PublisherRepository;
import info.mermakov.dev.library.service.PublisherService;
import info.mermakov.dev.library.service.book.BookServiceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.*;

import static info.mermakov.dev.library.service.book.BookServiceTestUtil.*;
import static info.mermakov.dev.library.service.publisher.PublisherServiceTestUtil.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DisplayName("Publisher Service Tests")
public class PublisherServiceTest {
    @MockBean
    private PublisherRepository repository;

    @Autowired
    private PublisherService publisherService;

    private List<Publisher> publishers;

    @BeforeEach
    public void init() {
        PublisherServiceTestUtil.initData();
        BookServiceTestUtil.initData();
        publishers = new ArrayList<>();
        publishers.add(PUBLISHER_1);
        publishers.add(PUBLISHER_2);
        Set<Book> books = new HashSet<>();
        books.add(BOOK_1);
        books.add(BOOK_2);
        PUBLISHER_1.setBooks(books);
    }

    @Test
    public void findAllPublishersTest() {
        Mockito.when(repository.findAll()).thenReturn(publishers);
        List<PublisherDto> result = publisherService.findAllPublishers();
        assertEquals(2, result.size());
        assertTrue(result.contains(PUBLISHER_DTO_1));
        assertTrue(result.contains(PUBLISHER_DTO_2));
    }

    @Test
    public void findAllPublishersExceptionTest() {
        Mockito.when(repository.findAll()).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> publisherService.findAllPublishers());
    }

    @Test
    public void findPublisherByIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(PUBLISHER_1));
        PublisherDto result = publisherService.findPublisherById(1L);
        assertEquals(PUBLISHER_DTO_1, result);
    }

    @Test
    public void findPublisherByIdExceptionTest() {
        Mockito.when(repository.findById(2L)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> publisherService.findPublisherById(2L));
    }

    @Test
    public void getPublisherOrNullByNameTest() {
        Mockito.when(repository.findByName("Publisher 1")).thenReturn(Optional.of(PUBLISHER_1));
        Mockito.when(repository.findByName("Publisher 2")).thenReturn(Optional.empty());
        assertEquals(PUBLISHER_1, publisherService.getPublisherOrNullByName("Publisher 1"));
        assertNull(publisherService.getPublisherOrNullByName("Publisher 2"));
    }

    @Test
    public void findPublishersByNameTest() {
        Mockito.when(repository.findByNameContains("Publisher")).thenReturn(publishers);
        List<PublisherDto> result = publisherService.findPublishersByName("Publisher");
        assertEquals(2, result.size());
        assertTrue(result.contains(PUBLISHER_DTO_1));
        assertTrue(result.contains(PUBLISHER_DTO_2));
    }

    @Test
    public void findPublishersByNameExceptionTest() {
        assertThrows(InvalidRequestException.class, () -> publisherService.findPublishersByName(null));
        Mockito.when(repository.findByNameContains("Publisher")).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> publisherService.findPublishersByName("Publisher"));
    }

    @Test
    public void findBooksByPublisherIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(PUBLISHER_1));
        List<BookDto> result = publisherService.findBooksByPublisherId(1L);
        assertEquals(2, result.size());
        assertTrue(result.contains(BOOK_DTO_1));
        assertTrue(result.contains(BOOK_DTO_2));
    }

    @Test
    public void findBooksByPublisherIdExceptionTest() {
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(PUBLISHER_2));
        assertThrows(ResourceNotFoundException.class, () -> publisherService.findBooksByPublisherId(2L));
    }

    @Test
    public void saveTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(PUBLISHER_1));
        Mockito.when(repository.findByName("Publisher 2")).thenReturn(Optional.empty());
        Mockito.when(repository.saveAndFlush(PUBLISHER_1)).thenReturn(PUBLISHER_1);
        PublisherDto publisher = new PublisherDto(1L, "Publisher 2", "Address 1");
        PublisherDto result = publisherService.save(publisher);
        assertEquals(publisher, result);
    }

    @Test
    public void saveNewTest() {
        Mockito.when(repository.findByName("Publisher 1")).thenReturn(Optional.empty());
        Publisher publisher = new Publisher();
        BeanUtils.copyProperties(PUBLISHER_1, publisher, "id");
        Mockito.when(repository.saveAndFlush(publisher)).thenReturn(PUBLISHER_1);
        PublisherDto publisherDto = new PublisherDto(null, "Publisher 1", "Address 1");
        PublisherDto result = publisherService.save(publisherDto);
        assertEquals(PUBLISHER_DTO_1, result);
    }

    @Test
    public void saveExceptionTest() {
        Mockito.when(repository.findByName("Publisher 2")).thenReturn(Optional.of(PUBLISHER_2));
        PublisherDto publisher = new PublisherDto(null, "Publisher 2", "Address 3");
        assertThrows(InvalidRequestException.class, () -> publisherService.save(publisher));
    }

    @Test
    public void removePublisherByIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(PUBLISHER_1));
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(PUBLISHER_2));
        publisherService.removePublisherById(2L);
        assertThrows(InvalidRequestException.class, () -> publisherService.removePublisherById(1L));
    }
}
