package info.mermakov.dev.library.service.rent;

import info.mermakov.dev.library.dto.BookRentDto;
import info.mermakov.dev.library.model.BookRent;

import java.time.LocalDate;

import static info.mermakov.dev.library.service.book.BookServiceTestUtil.*;
import static info.mermakov.dev.library.service.client.ClientServiceTestUtil.*;

public class BookRentServiceTestUtil {
    public static final BookRent RENT_1;
    public static final BookRent RENT_2;
    public static final BookRent RENT_3;
    public static final BookRentDto RENT_DTO_1;
    public static final BookRentDto RENT_DTO_2;
    public static final BookRentDto RENT_DTO_3;

    static {
        RENT_1 = new BookRent();
        RENT_2 = new BookRent();
        RENT_3 = new BookRent();
        RENT_DTO_1 = new BookRentDto();
        RENT_DTO_2 = new BookRentDto();
        RENT_DTO_3 = new BookRentDto();
        initData();
    }

    public static void initData() {
        RENT_1.setId(1L);
        RENT_1.setClient(CLIENT_1);
        RENT_1.setBook(BOOK_1);
        RENT_1.setIssueDate(LocalDate.now().minusDays(120));
        RENT_1.setReturnDate(null);

        RENT_DTO_1.setId(1L);
        RENT_DTO_1.setClient(CLIENT_DTO_1);
        RENT_DTO_1.setBook(BOOK_DTO_1);
        RENT_DTO_1.setIssueDate(LocalDate.now().minusDays(120));
        RENT_DTO_1.setReturnDate(null);

        RENT_2.setId(2L);
        RENT_2.setClient(CLIENT_1);
        RENT_2.setBook(BOOK_1);
        RENT_2.setIssueDate(LocalDate.now().minusDays(5));
        RENT_2.setReturnDate(null);

        RENT_DTO_2.setId(2L);
        RENT_DTO_2.setClient(CLIENT_DTO_1);
        RENT_DTO_2.setBook(BOOK_DTO_1);
        RENT_DTO_2.setIssueDate(LocalDate.now().minusDays(5));
        RENT_DTO_2.setReturnDate(null);

        RENT_3.setId(3L);
        RENT_3.setClient(CLIENT_2);
        RENT_3.setBook(BOOK_2);
        RENT_3.setIssueDate(LocalDate.of(2019, 4, 15));
        RENT_3.setReturnDate(LocalDate.of(2019, 8, 5));

        RENT_DTO_3.setId(3L);
        RENT_DTO_3.setClient(CLIENT_DTO_2);
        RENT_DTO_3.setBook(BOOK_DTO_2);
        RENT_DTO_3.setIssueDate(LocalDate.of(2019, 4, 15));
        RENT_DTO_3.setReturnDate(LocalDate.of(2019, 8, 5));
    }
}
