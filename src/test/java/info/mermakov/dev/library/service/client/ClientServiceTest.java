package info.mermakov.dev.library.service.client;

import info.mermakov.dev.library.dto.BookRentDto;
import info.mermakov.dev.library.dto.ClientDto;
import info.mermakov.dev.library.exception.InvalidRequestException;
import info.mermakov.dev.library.exception.ResourceNotFoundException;
import info.mermakov.dev.library.model.BookRent;
import info.mermakov.dev.library.model.Client;
import info.mermakov.dev.library.repository.BookRentRepository;
import info.mermakov.dev.library.repository.ClientRepository;
import info.mermakov.dev.library.service.ClientService;
import info.mermakov.dev.library.service.rent.BookRentServiceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.*;

import static info.mermakov.dev.library.service.client.ClientServiceTestUtil.*;
import static info.mermakov.dev.library.service.rent.BookRentServiceTestUtil.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DisplayName("Client Service Test")
public class ClientServiceTest {
    @MockBean
    private ClientRepository repository;
    @MockBean
    private BookRentRepository rentRepository;

    @Autowired
    private ClientService clientService;

    private List<Client> clients;

    @BeforeEach
    public void init() {
        clients = new ArrayList<>();
        clients.add(CLIENT_1);
        clients.add(CLIENT_2);
        ClientServiceTestUtil.initData();
        BookRentServiceTestUtil.initData();
        Set<BookRent> rents = new HashSet<>();
        rents.add(RENT_1);
        rents.add(RENT_2);
        Set<BookRent> rents2 = new HashSet<>();
        rents2.add(RENT_3);
        CLIENT_2.setRents(rents2);
        CLIENT_1.setRents(rents);
    }

    @Test
    public void findAllClientsTest() {
        Mockito.when(repository.findAll()).thenReturn(clients);
        List<ClientDto> result = clientService.findAllClients();
        assertEquals(2, result.size());
        assertTrue(result.contains(CLIENT_DTO_1));
        assertTrue(result.contains(CLIENT_DTO_2));
    }

    @Test
    public void findAllClientsExceptionTest() {
        Mockito.when(repository.findAll()).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> clientService.findAllClients());
    }

    @Test
    public void findClientByIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(CLIENT_1));
        assertEquals(CLIENT_DTO_1, clientService.findClientById(1L));
    }

    @Test
    public void findClientByIdExceptionTest() {
        Mockito.when(repository.findById(2L)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> clientService.findClientById(2L));
    }

    @Test
    public void findClientByEmailTest() {
        Mockito.when(repository.findByEmail("test@test.test")).thenReturn(Optional.of(CLIENT_1));
        assertEquals(CLIENT_DTO_1, clientService.findClientByEmail("test@test.test"));
    }

    @Test
    public void findClientByEmailExceptionTest() {
        assertThrows(InvalidRequestException.class, () -> clientService.findClientByEmail(null));
        Mockito.when(repository.findByEmail("mail@mail.mail")).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> clientService.findClientByEmail("mail@mail.mail"));
    }

    @Test
    public void findClientByPhoneTest() {
        Mockito.when(repository.findByPhone("+71231231231")).thenReturn(Optional.of(CLIENT_1));
        assertEquals(CLIENT_DTO_1, clientService.findClientByPhone("+71231231231"));
    }

    @Test
    public void findClientByPhoneExceptionTest() {
        assertThrows(InvalidRequestException.class, () -> clientService.findClientByPhone(null));
        Mockito.when(repository.findByPhone("+7123123")).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> clientService.findClientByPhone("+7123123"));
    }

    @Test
    public void findClientByNameTest() {
        List<Client> clients1 = new ArrayList<>();
        clients1.add(CLIENT_1);
        List<Client> clients2 = new ArrayList<>();
        clients2.add(CLIENT_1);
        Mockito.when(repository.findByFirstNameIsNotNullAndFirstNameContains("I")).thenReturn(clients1);
        Mockito.when(repository.findByMiddleNameIsNotNullAndMiddleNameContains("Iva")).thenReturn(clients2);
        Mockito.when(repository.findByLastNameIsNotNullAndLastNameContains("ov")).thenReturn(clients);
        List<ClientDto> result = clientService.findClientByName("I", "Iva", "ov");
        assertEquals(2, result.size());
        assertTrue(result.contains(CLIENT_DTO_1));
        assertTrue(result.contains(CLIENT_DTO_2));
    }

    @Test
    public void findClientByNameExceptionTest() {
        Mockito.when(repository.findByFirstNameIsNotNullAndFirstNameContains("I")).thenReturn(new ArrayList<>());
        Mockito.when(repository.findByMiddleNameIsNotNullAndMiddleNameContains("Iva")).thenReturn(new ArrayList<>());
        Mockito.when(repository.findByLastNameIsNotNullAndLastNameContains("ov")).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> clientService.findClientByName("I", "Iva", "ov"));
    }

    @Test
    public void findRentsByClientIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(CLIENT_1));
        List<BookRentDto> result = clientService.findRentsByClientId(1L);
        assertEquals(2, result.size());
        assertTrue(result.contains(RENT_DTO_1));
        assertTrue(result.contains(RENT_DTO_2));
    }

    @Test
    public void findRentsByClientIdExceptionTest() {
        CLIENT_1.setRents(new HashSet<>());
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(CLIENT_1));
        assertThrows(ResourceNotFoundException.class, () -> clientService.findRentsByClientId(1L));
    }

    @Test
    public void saveTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(CLIENT_1));
        Mockito.when(repository.findByPhone("+7124124")).thenReturn(Optional.empty());
        Mockito.when(repository.findByEmail("test@test.test")).thenReturn(Optional.of(CLIENT_1));
        Mockito.when(repository.saveAndFlush(CLIENT_1)).thenReturn(CLIENT_1);
        ClientDto clientDto = new ClientDto(1L,
                "Petr", null, "Petrov",
                "test@test.test", "+7124124", "Address 1");
        ClientDto result = clientService.save(clientDto);
        assertEquals(clientDto, result);
    }

    @Test
    public void saveNewTest() {
        Mockito.when(repository.findByEmail("")).thenReturn(Optional.empty());
        Mockito.when(repository.findByPhone("")).thenReturn(Optional.empty());
        Client client = new Client();
        BeanUtils.copyProperties(CLIENT_2, client, "id", "rents");
        Mockito.when(repository.saveAndFlush(client)).thenReturn(CLIENT_2);
        ClientDto clientDto = new ClientDto(null, "Petr", null, "Petrov",
                "test2@test.test", "+7123123", "Address 1");
        assertEquals(CLIENT_DTO_2, clientService.save(clientDto));
    }

    @Test
    public void saveExceptionTest() {
        Mockito.when(repository.findByEmail("test@test.test")).thenReturn(Optional.of(CLIENT_1));
        Mockito.when(repository.findByEmail("test2@test.test")).thenReturn(Optional.empty());
        Mockito.when(repository.findByPhone("+71231231231")).thenReturn(Optional.of(CLIENT_1));
        ClientDto clientDto = new ClientDto(null, "Petr", null, "Petrov",
                "test@test.test", "+71231231231", "Address 1");
        assertThrows(InvalidRequestException.class, () -> clientService.save(clientDto));
        clientDto.setEmail("test2@test.test");
        assertThrows(InvalidRequestException.class, () -> clientService.save(clientDto));
    }

    @Test
    public void removeClientByIdTest() {
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(CLIENT_2));
        Mockito.when(rentRepository.findById(3L)).thenReturn(Optional.of(RENT_3));
        clientService.removeClientById(2L);
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(CLIENT_1));
        assertThrows(InvalidRequestException.class, () -> clientService.removeClientById(1L));
    }
}
