package info.mermakov.dev.library.service.type;

import info.mermakov.dev.library.dto.TypeDto;
import info.mermakov.dev.library.model.Type;

import java.util.HashSet;

public class TypeServiceTestUtil {
    public static final Type TYPE_1;
    public static final Type TYPE_2;
    public static final TypeDto TYPE_DTO_1;
    public static final TypeDto TYPE_DTO_2;

    static {
        TYPE_1 = new Type();
        TYPE_2 = new Type();

        TYPE_DTO_1 = new TypeDto();
        TYPE_DTO_2 = new TypeDto();
        initData();
    }

    public static void initData() {
        TYPE_1.setId(1L);
        TYPE_1.setName("Type 1");
        TYPE_1.setBooks(new HashSet<>());

        TYPE_2.setId(2L);
        TYPE_2.setName("Type 2");
        TYPE_2.setBooks(new HashSet<>());

        TYPE_DTO_1.setId(1L);
        TYPE_DTO_1.setName("Type 1");

        TYPE_DTO_2.setId(2L);
        TYPE_DTO_2.setName("Type 2");
    }
}
