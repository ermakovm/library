package info.mermakov.dev.library.service.book;

import info.mermakov.dev.library.dto.AuthorDto;
import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.dto.CategoryDto;
import info.mermakov.dev.library.model.Author;
import info.mermakov.dev.library.model.Book;
import info.mermakov.dev.library.model.Category;

import java.time.Year;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static info.mermakov.dev.library.service.author.AuthorServiceTestUtil.*;
import static info.mermakov.dev.library.service.category.CategoryServiceTestUtil.*;
import static info.mermakov.dev.library.service.publisher.PublisherServiceTestUtil.PUBLISHER_1;
import static info.mermakov.dev.library.service.publisher.PublisherServiceTestUtil.PUBLISHER_DTO_1;
import static info.mermakov.dev.library.service.type.TypeServiceTestUtil.TYPE_1;
import static info.mermakov.dev.library.service.type.TypeServiceTestUtil.TYPE_DTO_1;

public class BookServiceTestUtil {
    public static final Book BOOK_1;
    public static final Book BOOK_2;
    public static final BookDto BOOK_DTO_1;
    public static final BookDto BOOK_DTO_2;

    static {
        BOOK_1 = new Book();
        BOOK_2 = new Book();
        BOOK_DTO_1 = new BookDto();
        BOOK_DTO_2 = new BookDto();
        initData();
    }

    public static void initData() {
        Set<Author> authors = new HashSet<>();
        authors.add(AUTHOR_1);
        authors.add(AUTHOR_2);

        Set<Category> categories = new HashSet<>();
        categories.add(CATEGORY_1);
        categories.add(CATEGORY_2);

        List<AuthorDto> authorsDto = new ArrayList<>();
        authorsDto.add(AUTHOR_DTO_1);
        authorsDto.add(AUTHOR_DTO_2);

        List<CategoryDto> categoriesDto = new ArrayList<>();
        categoriesDto.add(CATEGORY_DTO_1);
        categoriesDto.add(CATEGORY_DTO_2);

        BOOK_1.setId(1L);
        BOOK_1.setTitle("Book 1");
        BOOK_1.setIsbn("978-3-16-148410-0");
        BOOK_1.setRelease(Year.of(2017));
        BOOK_1.setEdition(1);
        BOOK_1.setType(TYPE_1);
        BOOK_1.setPublisher(PUBLISHER_1);
        BOOK_1.setAuthors(authors);
        BOOK_1.setCategories(categories);

        BOOK_2.setId(2L);
        BOOK_2.setTitle("Book 2");
        BOOK_2.setRelease(Year.of(2015));
        BOOK_2.setType(TYPE_1);
        BOOK_2.setPublisher(PUBLISHER_1);
        BOOK_2.setAuthors(authors);
        BOOK_2.setCategories(categories);

        BOOK_DTO_1.setId(1L);
        BOOK_DTO_1.setTitle("Book 1");
        BOOK_DTO_1.setIsbn("978-3-16-148410-0");
        BOOK_DTO_1.setRelease(Year.of(2017));
        BOOK_DTO_1.setEdition(1);
        BOOK_DTO_1.setType(TYPE_DTO_1);
        BOOK_DTO_1.setPublisher(PUBLISHER_DTO_1);
        BOOK_DTO_1.setAuthors(authorsDto);
        BOOK_DTO_1.setCategories(categoriesDto);

        BOOK_DTO_2.setId(2L);
        BOOK_DTO_2.setTitle("Book 2");
        BOOK_DTO_2.setRelease(Year.of(2015));
        BOOK_DTO_2.setType(TYPE_DTO_1);
        BOOK_DTO_2.setPublisher(PUBLISHER_DTO_1);
        BOOK_DTO_2.setAuthors(authorsDto);
        BOOK_DTO_2.setCategories(categoriesDto);
    }
}