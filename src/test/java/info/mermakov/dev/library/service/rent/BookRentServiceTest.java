package info.mermakov.dev.library.service.rent;

import info.mermakov.dev.library.dto.BookRentDto;
import info.mermakov.dev.library.dto.SimpleBookRentDto;
import info.mermakov.dev.library.exception.InvalidRequestException;
import info.mermakov.dev.library.exception.ResourceNotFoundException;
import info.mermakov.dev.library.model.BookRent;
import info.mermakov.dev.library.repository.BookRentRepository;
import info.mermakov.dev.library.repository.BookRepository;
import info.mermakov.dev.library.repository.ClientRepository;
import info.mermakov.dev.library.service.BookRentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static info.mermakov.dev.library.service.book.BookServiceTestUtil.BOOK_1;
import static info.mermakov.dev.library.service.book.BookServiceTestUtil.BOOK_2;
import static info.mermakov.dev.library.service.client.ClientServiceTestUtil.CLIENT_1;
import static info.mermakov.dev.library.service.client.ClientServiceTestUtil.CLIENT_2;
import static info.mermakov.dev.library.service.rent.BookRentServiceTestUtil.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class BookRentServiceTest {
    @MockBean
    private BookRentRepository repository;
    @MockBean
    private ClientRepository clientRepository;
    @MockBean
    private BookRepository bookRepository;

    @Value("${days.for.return}")
    private int daysForReturn;

    @Autowired
    private BookRentService rentService;

    private List<BookRent> rents;

    @BeforeEach
    public void init() {
        rents = new ArrayList<>();
        rents.add(RENT_1);
        rents.add(RENT_2);
        rents.add(RENT_3);
        initData();
        Mockito.when(clientRepository.findById(1L)).thenReturn(Optional.of(CLIENT_1));
        Mockito.when(clientRepository.findById(2L)).thenReturn(Optional.of(CLIENT_2));
        Mockito.when(bookRepository.findById(1L)).thenReturn(Optional.of(BOOK_1));
        Mockito.when(bookRepository.findById(2L)).thenReturn(Optional.of(BOOK_2));
    }

    @Test
    public void findAllRentsTest() {
        Mockito.when(repository.findAll()).thenReturn(rents);
        List<BookRentDto> result = rentService.findAllRents();
        assertEquals(3, result.size());
        assertTrue(result.contains(RENT_DTO_1));
        assertTrue(result.contains(RENT_DTO_3));
    }

    @Test
    public void findAllRentsExceptionTest() {
        Mockito.when(repository.findAll()).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> rentService.findAllRents());
    }

    @Test
    public void findRentByIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(RENT_1));
        assertEquals(RENT_DTO_1, rentService.findRentById(1L));
    }

    @Test
    public void findRentByIdExceptionTest() {
        Mockito.when(repository.findById(2L)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> rentService.findRentById(2L));
    }

    @Test
    public void findNotReturnedRentsTest() {
        List<BookRent> notReturned = new ArrayList<>();
        notReturned.add(RENT_1);
        notReturned.add(RENT_2);
        Mockito.when(repository.findByReturnDateIsNull()).thenReturn(notReturned);
        List<BookRentDto> result = rentService.findNotReturnedRents();
        assertEquals(2, result.size());
        assertTrue(result.contains(RENT_DTO_1));
        assertTrue(result.contains(RENT_DTO_2));
    }

    @Test
    public void findNotReturnedRentsExceptionTest() {
        Mockito.when(repository.findByReturnDateIsNull()).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> rentService.findNotReturnedRents());
    }

    @Test
    public void findExpiredRentsOrNullTest() {
        List<BookRent> expiredRents = new ArrayList<>();
        expiredRents.add(RENT_1);
        LocalDate date = LocalDate.now();
        Mockito.when(repository.findByReturnDateIsNullAndIssueDateBefore(date.minusDays(daysForReturn))).thenReturn(expiredRents);
        List<BookRentDto> result = rentService.findExpiredRentsOrNull(date);
        assertEquals(1, result.size());
        assertTrue(result.contains(RENT_DTO_1));
        Mockito.when(repository.findByReturnDateIsNullAndIssueDateBefore(date.minusDays(daysForReturn))).thenReturn(new ArrayList<>());
        assertNull(rentService.findExpiredRentsOrNull(date));
    }

    @Test
    public void saveTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(RENT_1));
        Mockito.when(repository.saveAndFlush(RENT_1)).thenReturn(RENT_1);
        SimpleBookRentDto rentDto = new SimpleBookRentDto(1L, 1L, 1L, LocalDate.now().minusDays(120), LocalDate.now());
        RENT_DTO_1.setReturnDate(LocalDate.now());
        assertEquals(RENT_DTO_1, rentService.save(rentDto));
    }

    @Test
    public void saveNewTest() {
        BookRent rent = new BookRent();
        BeanUtils.copyProperties(RENT_2, rent, "id");
        Mockito.when(repository.saveAndFlush(rent)).thenReturn(RENT_2);
        SimpleBookRentDto rentDto = new SimpleBookRentDto(null, 1L, 1L, LocalDate.now().minusDays(5), null);
        BookRentDto result = rentService.save(rentDto);
        assertEquals(RENT_DTO_2, result);
    }

    @Test
    public void saveExceptionTest() {
        List<BookRent> bookRents = new ArrayList<>();
        bookRents.add(RENT_1);
        bookRents.add(RENT_2);
        Mockito.when(repository.findByClientId(1L)).thenReturn(bookRents);
        SimpleBookRentDto rentDto = new SimpleBookRentDto(null, 1L, 1L, LocalDate.now().minusDays(120), null);
        assertThrows(InvalidRequestException.class, () -> rentService.save(rentDto));
    }

    @Test
    public void removeRentByIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(RENT_1));
        Mockito.when(repository.findById(3L)).thenReturn(Optional.of(RENT_3));
        rentService.removeRentById(3L);
        assertThrows(InvalidRequestException.class, () -> rentService.removeRentById(1L));
    }

    /*

    @Test
    public void saveTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(rent1));
        Mockito.when(repository.saveAndFlush(rent1)).thenReturn(rent1);
        SimpleBookRentDto rentDto = new SimpleBookRentDto(1L, 1L, 1L, LocalDate.now().minusDays(120), LocalDate.now());
        rentDto1.setReturnDate(LocalDate.now());
        BookRentDto result = rentService.save(rentDto);
        assertEquals(rentDto1, result);
    }

    @Test
    public void saveNewTest() {
        BookRent rent = new BookRent();
        BeanUtils.copyProperties(rent2, rent, "id");
        Mockito.when(repository.saveAndFlush(rent)).thenReturn(rent2);
        SimpleBookRentDto rentDto = new SimpleBookRentDto(null, 1L, 1L, LocalDate.now().minusDays(5), null);
        BookRentDto result = rentService.save(rentDto);
        assertEquals(rentDto2, result);
    }
*/
}
