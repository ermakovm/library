package info.mermakov.dev.library.service.book;

import info.mermakov.dev.library.dto.*;
import info.mermakov.dev.library.exception.InvalidRequestException;
import info.mermakov.dev.library.exception.ResourceNotFoundException;
import info.mermakov.dev.library.model.*;
import info.mermakov.dev.library.repository.*;
import info.mermakov.dev.library.service.BookService;
import info.mermakov.dev.library.service.author.AuthorServiceTestUtil;
import info.mermakov.dev.library.service.category.CategoryServiceTestUtil;
import info.mermakov.dev.library.service.publisher.PublisherServiceTestUtil;
import info.mermakov.dev.library.service.rent.BookRentServiceTestUtil;
import info.mermakov.dev.library.service.type.TypeServiceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.Year;
import java.util.*;

import static info.mermakov.dev.library.service.author.AuthorServiceTestUtil.*;
import static info.mermakov.dev.library.service.book.BookServiceTestUtil.*;
import static info.mermakov.dev.library.service.category.CategoryServiceTestUtil.*;
import static info.mermakov.dev.library.service.publisher.PublisherServiceTestUtil.PUBLISHER_1;
import static info.mermakov.dev.library.service.publisher.PublisherServiceTestUtil.PUBLISHER_DTO_1;
import static info.mermakov.dev.library.service.rent.BookRentServiceTestUtil.*;
import static info.mermakov.dev.library.service.type.TypeServiceTestUtil.TYPE_1;
import static info.mermakov.dev.library.service.type.TypeServiceTestUtil.TYPE_DTO_1;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class BookServiceTest {
    @MockBean
    private BookRepository repository;
    @MockBean
    private AuthorRepository authorRepository;
    @MockBean
    private CategoryRepository categoryRepository;
    @MockBean
    private PublisherRepository publisherRepository;
    @MockBean
    private TypeRepository typeRepository;
    @MockBean
    private BookRentRepository rentRepository;

    @Autowired
    private BookService bookService;

    private List<Book> books;

    @BeforeEach
    public void init() {
        books = new ArrayList<>();
        books.add(BOOK_1);
        books.add(BOOK_2);
        BookServiceTestUtil.initData();
        AuthorServiceTestUtil.initData();
        CategoryServiceTestUtil.initData();
        PublisherServiceTestUtil.initData();
        TypeServiceTestUtil.initData();
        BookRentServiceTestUtil.initData();

        Set<BookRent> rents = new HashSet<>();
        rents.add(RENT_1);
        rents.add(RENT_2);
        Set<BookRent> rents1 = new HashSet<>();
        rents1.add(RENT_3);
        BOOK_1.setRents(rents);
        BOOK_2.setRents(rents1);
    }

    @Test
    public void findAllBooksTest() {
        Mockito.when(repository.findAll()).thenReturn(books);
        List<BookDto> result = bookService.findAllBooks();
        assertEquals(2, result.size());
        assertTrue(result.contains(BOOK_DTO_1));
        assertTrue(result.contains(BOOK_DTO_2));
    }

    @Test
    public void findAllBooksExceptionTest() {
        Mockito.when(repository.findAll()).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> bookService.findAllBooks());
    }

    @Test
    public void findBookByIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(BOOK_1));
        assertEquals(BOOK_DTO_1, bookService.findBookById(1L));
    }

    @Test
    public void findBookByIdExceptionTest() {
        Mockito.when(repository.findById(2L)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> bookService.findBookById(2L));
    }

    @Test
    public void findBooksByTitleTest() {
        Mockito.when(repository.findByTitleContains("Book")).thenReturn(books);
        List<BookDto> result = bookService.findBooksByTitle("Book");
        assertEquals(2, result.size());
        assertTrue(result.contains(BOOK_DTO_1));
        assertTrue(result.contains(BOOK_DTO_2));
    }

    @Test
    public void findBooksByTitleExceptionTest() {
        Mockito.when(repository.findByTitleContains("Book")).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> bookService.findBooksByTitle("Book"));
        assertThrows(InvalidRequestException.class, () -> bookService.findBooksByTitle(null));
    }

    @Test
    public void findRentByBookIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(BOOK_1));
        List<BookRentDto> result = bookService.findRentByBookId(1L);
        assertEquals(2, result.size());
        assertTrue(result.contains(RENT_DTO_1));
        assertTrue(result.contains(RENT_DTO_2));
    }

    @Test
    public void findRentByBookIdExceptionTest() {
        BOOK_2.setRents(new HashSet<>());
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(BOOK_2));
        assertThrows(ResourceNotFoundException.class, () -> bookService.findRentByBookId(2L));
    }

    @Test
    public void saveTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(BOOK_1));
        Mockito.when(repository.findByIsbn("978-3-16-148410-0")).thenReturn(Optional.of(BOOK_1));
        Mockito.when(repository.saveAndFlush(BOOK_1)).thenReturn(BOOK_1);
        Mockito.when(typeRepository.findByName("Type 1")).thenReturn(Optional.of(TYPE_1));
        Mockito.when(publisherRepository.findByName("Publisher 1")).thenReturn(Optional.of(PUBLISHER_1));
        Mockito.when(authorRepository.findByFirstNameAndMiddleNameAndLastName("Ivan", "Ivanovich", "Ivanov"))
                .thenReturn(Optional.of(AUTHOR_1));
        Mockito.when(authorRepository.findByFirstNameAndMiddleNameAndLastName("Petr", null, "Petrov"))
                .thenReturn(Optional.of(AUTHOR_2));
        Mockito.when(categoryRepository.findByName("Category 1")).thenReturn(Optional.of(CATEGORY_1));
        Mockito.when(categoryRepository.findByName("Category 2")).thenReturn(Optional.of(CATEGORY_2));

        BookDto bookDto = new BookDto();
        bookDto.setId(1L);
        bookDto.setTitle("New Book Title");
        bookDto.setIsbn("978-3-16-148410-0");
        bookDto.setRelease(Year.of(2019));
        bookDto.setEdition(3);
        bookDto.setType(TYPE_DTO_1);
        bookDto.setPublisher(PUBLISHER_DTO_1);
        bookDto.setAuthors(BOOK_DTO_1.getAuthors());
        bookDto.setCategories(BOOK_DTO_1.getCategories());

        BookDto result = bookService.save(bookDto);
        assertEquals(bookDto, result);
    }

    private void setDependencies(BookDto bookDto) {
        TypeDto typeDto = new TypeDto();
        BeanUtils.copyProperties(TYPE_DTO_1, typeDto, "id");

        PublisherDto publisherDto = new PublisherDto();
        BeanUtils.copyProperties(PUBLISHER_DTO_1, publisherDto, "id");

        AuthorDto author1 = new AuthorDto();
        BeanUtils.copyProperties(AUTHOR_DTO_1, author1, "id");

        AuthorDto author2 = new AuthorDto();
        BeanUtils.copyProperties(AUTHOR_DTO_2, author2, "id");

        CategoryDto category1 = new CategoryDto();
        BeanUtils.copyProperties(CATEGORY_DTO_1, category1, "id");

        CategoryDto category2 = new CategoryDto();
        BeanUtils.copyProperties(CATEGORY_DTO_2, category2, "id");

        List<CategoryDto> categoriesDto = new ArrayList<>();
        categoriesDto.add(category1);
        categoriesDto.add(category2);

        List<AuthorDto> authorsDto = new ArrayList<>();
        authorsDto.add(author1);
        authorsDto.add(author2);

        bookDto.setType(typeDto);
        bookDto.setPublisher(publisherDto);
        bookDto.setAuthors(authorsDto);
        bookDto.setCategories(categoriesDto);
    }

    @Test
    public void saveNewAndCheckExistsTest() {
        Mockito.when(repository.findByIsbn("978-3-16-148410-0")).thenReturn(Optional.empty());
        Book book = new Book();
        BeanUtils.copyProperties(BOOK_1, book, "id");
        Mockito.when(repository.saveAndFlush(book)).thenReturn(BOOK_1);

        Mockito.when(typeRepository.findByName("Type 1")).thenReturn(Optional.empty());
        Type type = new Type();
        BeanUtils.copyProperties(TYPE_1, type, "id", "books");
        Mockito.when(typeRepository.saveAndFlush(type)).thenReturn(TYPE_1);

        Mockito.when(publisherRepository.findByName("Publisher 1")).thenReturn(Optional.empty());
        Publisher publisher = new Publisher();
        BeanUtils.copyProperties(PUBLISHER_1, publisher, "id", "books");
        Mockito.when(publisherRepository.saveAndFlush(publisher)).thenReturn(PUBLISHER_1);

        Mockito.when(categoryRepository.findByName("Category 1")).thenReturn(Optional.of(CATEGORY_1));
        Mockito.when(categoryRepository.findByName("Category 2")).thenReturn(Optional.empty());
        Category category = new Category();
        category.setName("Category 2");
        Mockito.when(categoryRepository.saveAndFlush(category)).thenReturn(CATEGORY_2);

        Mockito.when(authorRepository.findByFirstNameAndMiddleNameAndLastName("Ivan", "Ivanovich", "Ivanov"))
                .thenReturn(Optional.of(AUTHOR_1));
        Mockito.when(authorRepository.findByFirstNameAndMiddleNameAndLastName("Petr", null, "Petrov"))
                .thenReturn(Optional.empty());
        Author author = new Author();
        author.setFirstName("Petr");
        author.setLastName("Petrov");
        Mockito.when(authorRepository.saveAndFlush(author)).thenReturn(AUTHOR_2);

        BookDto bookDto = new BookDto();
        bookDto.setTitle("Book 1");
        bookDto.setIsbn("978-3-16-148410-0");
        bookDto.setRelease(Year.of(2017));
        bookDto.setEdition(1);
        setDependencies(bookDto);

        BookDto result = bookService.save(bookDto);
        assertEquals(BOOK_DTO_1, result);

        List<Book> bookList = new ArrayList<>();
        bookList.add(BOOK_1);
        Mockito.when(repository.findByTitle("Book 1")).thenReturn(bookList);
        BOOK_DTO_1.setId(null);
        assertThrows(InvalidRequestException.class, () -> bookService.save(BOOK_DTO_1));
    }

    @Test
    public void saveExceptionTest() {
        Mockito.when(repository.findByIsbn("978-3-16-148410-0")).thenReturn(Optional.of(BOOK_1));
        BookDto bookDto = new BookDto();
        BeanUtils.copyProperties(BOOK_DTO_2, bookDto, "id");
        bookDto.setIsbn("978-3-16-148410-0");
        assertThrows(InvalidRequestException.class, () -> bookService.save(bookDto));
    }

    @Test
    public void removeBookByIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(BOOK_1));
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(BOOK_2));
        Mockito.when(rentRepository.findById(3L)).thenReturn(Optional.of(RENT_3));
        bookService.removeBookById(2L);
        assertThrows(InvalidRequestException.class, () -> bookService.removeBookById(1L));
    }
}