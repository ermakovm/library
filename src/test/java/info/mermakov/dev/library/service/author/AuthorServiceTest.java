package info.mermakov.dev.library.service.author;

import info.mermakov.dev.library.dto.AuthorDto;
import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.exception.InvalidRequestException;
import info.mermakov.dev.library.exception.ResourceNotFoundException;
import info.mermakov.dev.library.model.Author;
import info.mermakov.dev.library.model.Book;
import info.mermakov.dev.library.repository.AuthorRepository;
import info.mermakov.dev.library.service.AuthorService;
import info.mermakov.dev.library.service.book.BookServiceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.*;

import static info.mermakov.dev.library.service.author.AuthorServiceTestUtil.*;
import static info.mermakov.dev.library.service.book.BookServiceTestUtil.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DisplayName("Author Service Test")
public class AuthorServiceTest {
    @MockBean
    private AuthorRepository repository;

    @Autowired
    private AuthorService authorService;

    private List<Author> authors;

    @BeforeEach
    public void init() {
        authors = new ArrayList<>();
        authors.add(AUTHOR_1);
        authors.add(AUTHOR_2);
        AuthorServiceTestUtil.initData();
        BookServiceTestUtil.initData();
        Set<Book> books = new HashSet<>();
        books.add(BOOK_1);
        books.add(BOOK_2);
        AUTHOR_1.setBooks(books);
    }

    @Test
    public void findAllAuthorsTest() {
        Mockito.when(repository.findAll()).thenReturn(authors);
        List<AuthorDto> result = authorService.findAllAuthors();
        assertEquals(2, result.size());
        assertTrue(result.contains(AUTHOR_DTO_1));
        assertTrue(result.contains(AUTHOR_DTO_2));
    }

    @Test
    public void findAllAuthorsExceptionTest() {
        Mockito.when(repository.findAll()).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> authorService.findAllAuthors());
    }

    @Test
    public void findAuthorByIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(AUTHOR_1));
        assertEquals(AUTHOR_DTO_1, authorService.findAuthorById(1L));
    }

    @Test
    public void findAuthorByIdExceptionTest() {
        Mockito.when(repository.findById(2L)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> authorService.findAuthorById(2L));
    }

    @Test
    public void getAuthorOrNullByNameTest() {
        Mockito.when(repository.findByFirstNameAndMiddleNameAndLastName("Petr", null, "Petrov")).thenReturn(Optional.of(AUTHOR_2));
        Mockito.when(repository.findByFirstNameAndMiddleNameAndLastName("Ivan", "Ivanovich", "Ivanov")).thenReturn(Optional.empty());
        assertEquals(AUTHOR_2, authorService.getAuthorOrNullByName("Petr", null, "Petrov"));
        assertNull(authorService.getAuthorOrNullByName("Ivan", "Ivanovich", "Ivanov"));
    }

    @Test
    public void findAuthorsByNameTest() {
        List<Author> authors1 = new ArrayList<>();
        authors1.add(AUTHOR_1);
        List<Author> authors2 = new ArrayList<>();
        authors2.add(AUTHOR_1);
        Mockito.when(repository.findByFirstNameIsNotNullAndFirstNameContains("I")).thenReturn(authors1);
        Mockito.when(repository.findByMiddleNameIsNotNullAndMiddleNameContains("Iva")).thenReturn(authors2);
        Mockito.when(repository.findByLastNameIsNotNullAndLastNameContains("ov")).thenReturn(authors);
        List<AuthorDto> result = authorService.findAuthorsByName("I", "Iva", "ov");
        assertEquals(2, result.size());
        assertTrue(result.contains(AUTHOR_DTO_1));
        assertTrue(result.contains(AUTHOR_DTO_2));
    }

    @Test
    public void findAuthorsByNameExceptionTest() {
        Mockito.when(repository.findByFirstNameIsNotNullAndFirstNameContains("I")).thenReturn(new ArrayList<>());
        Mockito.when(repository.findByMiddleNameIsNotNullAndMiddleNameContains("Iva")).thenReturn(new ArrayList<>());
        Mockito.when(repository.findByLastNameIsNotNullAndLastNameContains("ov")).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> authorService.findAuthorsByName("I", "Iva", "ov"));
    }

    @Test
    public void findBooksByAuthorIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(AUTHOR_1));
        List<BookDto> result = authorService.findBooksByAuthorId(1L);
        assertEquals(2, result.size());
        assertTrue(result.contains(BOOK_DTO_1));
        assertTrue(result.contains(BOOK_DTO_2));
    }

    @Test
    public void findBooksByAuthorIdExceptionTest() {
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(AUTHOR_2));
        assertThrows(ResourceNotFoundException.class, () -> authorService.findBooksByAuthorId(2L));
    }

    @Test
    public void saveTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(AUTHOR_1));
        Mockito.when(repository.findByFirstNameAndMiddleNameAndLastName("Petr", "Ivanovich", "Ivanov")).thenReturn(Optional.empty());
        Mockito.when(repository.saveAndFlush(AUTHOR_1)).thenReturn(AUTHOR_1);
        AuthorDto authorDto = new AuthorDto(1L, "Petr", "Ivanovich", "Ivanov");
        AuthorDto result = authorService.save(authorDto);
        assertEquals(authorDto, result);
    }

    @Test
    public void saveNewTest() {
        Mockito.when(repository.findByFirstNameAndMiddleNameAndLastName("Ivan", "Ivanovich", "Ivanov")).thenReturn(Optional.empty());
        Author author = new Author();
        BeanUtils.copyProperties(AUTHOR_1, author, "id");
        Mockito.when(repository.saveAndFlush(author)).thenReturn(AUTHOR_1);
        AuthorDto authorDto = new AuthorDto(null, "Ivan", "Ivanovich", "Ivanov");
        assertEquals(AUTHOR_DTO_1, authorService.save(authorDto));
    }

    @Test
    public void saveExceptionTest() {
        Mockito.when(repository.findByFirstNameAndMiddleNameAndLastName("Ivan", "Ivanovich", "Ivanov")).thenReturn(Optional.of(AUTHOR_1));
        AuthorDto authorDto = new AuthorDto(null, "Ivan", "Ivanovich", "Ivanov");
        assertThrows(InvalidRequestException.class, () -> authorService.save(authorDto));
    }

    @Test
    public void removeAuthorByIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(AUTHOR_1));
        authorService.removeAuthorById(1L);
    }
}
