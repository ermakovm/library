package info.mermakov.dev.library.service.category;

import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.dto.CategoryDto;
import info.mermakov.dev.library.exception.InvalidRequestException;
import info.mermakov.dev.library.exception.ResourceNotFoundException;
import info.mermakov.dev.library.model.Book;
import info.mermakov.dev.library.model.Category;
import info.mermakov.dev.library.repository.CategoryRepository;
import info.mermakov.dev.library.service.CategoryService;
import info.mermakov.dev.library.service.book.BookServiceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.*;

import static info.mermakov.dev.library.service.book.BookServiceTestUtil.*;
import static info.mermakov.dev.library.service.category.CategoryServiceTestUtil.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class CategoryServiceTest {
    @MockBean
    private CategoryRepository repository;

    @Autowired
    private CategoryService categoryService;

    private List<Category> categories;

    @BeforeEach
    public void init() {
        categories = new ArrayList<>();
        categories.add(CATEGORY_1);
        categories.add(CATEGORY_2);
        CategoryServiceTestUtil.initData();
        BookServiceTestUtil.initData();
        Set<Book> books = new HashSet<>();
        books.add(BOOK_1);
        books.add(BOOK_2);
        CATEGORY_1.setBooks(books);
    }

    @Test
    public void findAllCategoriesTest() {
        Mockito.when(repository.findAll()).thenReturn(categories);
        List<CategoryDto> result = categoryService.findAllCategories();
        assertEquals(2, result.size());
        assertTrue(result.contains(CATEGORY_DTO_1));
        assertTrue(result.contains(CATEGORY_DTO_2));
    }

    @Test
    public void findAllCategoriesExceptionTest() {
        Mockito.when(repository.findAll()).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> categoryService.findAllCategories());
    }

    @Test
    public void findCategoryByIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(CATEGORY_1));
        assertEquals(CATEGORY_DTO_1, categoryService.findCategoryById(1L));
    }

    @Test
    public void findCategoryByIdExceptionTest() {
        Mockito.when(repository.findById(2L)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> categoryService.findCategoryById(2L));
    }

    @Test
    public void getCategoryOrNullByNameTest() {
        Mockito.when(repository.findByName("Category 1")).thenReturn(Optional.of(CATEGORY_1));
        Mockito.when(repository.findByName("Category 2")).thenReturn(Optional.empty());
        assertEquals(CATEGORY_1, categoryService.getCategoryOrNullByName("Category 1"));
        assertNull(categoryService.getCategoryOrNullByName("Category 2"));
    }

    @Test
    public void findCategoriesByNameTest() {
        Mockito.when(repository.findByNameContains("Category")).thenReturn(categories);
        List<CategoryDto> result = categoryService.findCategoriesByName("Category");
        assertEquals(2, result.size());
        assertTrue(result.contains(CATEGORY_DTO_1));
        assertTrue(result.contains(CATEGORY_DTO_2));
    }

    @Test
    public void findCategoriesByNameExceptionTest() {
        assertThrows(InvalidRequestException.class, () -> categoryService.findCategoriesByName(null));
        Mockito.when(repository.findByNameContains("Category 2")).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> categoryService.findCategoriesByName("Category 2"));
    }

    @Test
    public void findBooksByCategoryIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(CATEGORY_1));
        List<BookDto> result = categoryService.findBooksByCategoryId(1L);
        assertEquals(2, result.size());
        assertTrue(result.contains(BOOK_DTO_1));
        assertTrue(result.contains(BOOK_DTO_2));
    }

    @Test
    public void findBooksByCategoryIdExceptionTest() {
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(CATEGORY_2));
        assertThrows(ResourceNotFoundException.class, () -> categoryService.findBooksByCategoryId(2L));
    }

    @Test
    public void saveTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(CATEGORY_1));
        Mockito.when(repository.findByName("Category 2")).thenReturn(Optional.empty());
        Mockito.when(repository.saveAndFlush(CATEGORY_1)).thenReturn(CATEGORY_1);
        CategoryDto categoryDto = new CategoryDto(1L, "Category 3");
        CategoryDto result = categoryService.save(categoryDto);
        assertEquals(categoryDto, result);
    }

    @Test
    public void saveNewTest() {
        Mockito.when(repository.findByName("Category 1")).thenReturn(Optional.empty());
        Category category = new Category();
        BeanUtils.copyProperties(CATEGORY_1, category, "id");
        Mockito.when(repository.saveAndFlush(category)).thenReturn(CATEGORY_1);
        CategoryDto categoryDto = new CategoryDto(null, "Category 1");
        CategoryDto result = categoryService.save(categoryDto);
        assertEquals(CATEGORY_DTO_1, result);
    }

    @Test
    public void saveExceptionTest() {
        Mockito.when(repository.findByName("Category 2")).thenReturn(Optional.of(CATEGORY_2));
        CategoryDto categoryDto = new CategoryDto(null, "Category 2");
        assertThrows(InvalidRequestException.class, () -> categoryService.save(categoryDto));
    }

    @Test
    public void removeCategoryByIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(CATEGORY_1));
        categoryService.removeCategoryById(1L);
    }
}
