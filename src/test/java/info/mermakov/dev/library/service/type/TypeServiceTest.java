package info.mermakov.dev.library.service.type;

import info.mermakov.dev.library.dto.BookDto;
import info.mermakov.dev.library.dto.TypeDto;
import info.mermakov.dev.library.exception.InvalidRequestException;
import info.mermakov.dev.library.exception.ResourceNotFoundException;
import info.mermakov.dev.library.model.Book;
import info.mermakov.dev.library.model.Type;
import info.mermakov.dev.library.repository.TypeRepository;
import info.mermakov.dev.library.service.TypeService;
import info.mermakov.dev.library.service.book.BookServiceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.*;

import static info.mermakov.dev.library.service.book.BookServiceTestUtil.*;
import static info.mermakov.dev.library.service.type.TypeServiceTestUtil.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DisplayName("Type Service Tests")
public class TypeServiceTest {
    @Autowired
    private TypeService typeService;
    @MockBean
    private TypeRepository repository;

    private List<Type> types;

    @BeforeEach
    public void init() {
        types = new ArrayList<>();
        types.add(TYPE_1);
        types.add(TYPE_2);
        TypeServiceTestUtil.initData();
        BookServiceTestUtil.initData();
        Set<Book> books = new HashSet<>();
        books.add(BOOK_1);
        books.add(BOOK_2);
        TYPE_1.setBooks(books);
    }

    @Test
    public void findAllTypesTest() {
        Mockito.when(repository.findAll()).thenReturn(types);
        List<TypeDto> result = typeService.findAllTypes();
        assertEquals(2, result.size());
        assertTrue(result.contains(TYPE_DTO_1));
        assertTrue(result.contains(TYPE_DTO_2));
    }

    @Test
    public void findAllTypesExceptionTest() {
        Mockito.when(repository.findAll()).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> typeService.findAllTypes());
    }

    @Test
    public void findTypeByIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(TYPE_1));
        TypeDto result = typeService.findTypeById(1L);
        assertEquals(result, TYPE_DTO_1);
    }

    @Test
    public void findTypeByIdExceptionTest() {
        Mockito.when(repository.findById(5L)).thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> typeService.findTypeById(5L));
    }

    @Test
    public void getTypeOrNullByNameTest() {
        Mockito.when(repository.findByName("Type 1")).thenReturn(Optional.of(TYPE_1));
        Mockito.when(repository.findByName("Type 2")).thenReturn(Optional.empty());
        assertEquals(TYPE_1, typeService.getTypeOrNullByName("Type 1"));
        assertNull(typeService.getTypeOrNullByName("Type 2"));
    }

    @Test
    public void findTypesByNameTest() {
        Mockito.when(repository.findByNameContains("Type")).thenReturn(types);
        List<TypeDto> result = typeService.findTypesByName("Type");
        assertEquals(2, result.size());
        assertTrue(result.contains(TYPE_DTO_1));
        assertTrue(result.contains(TYPE_DTO_2));
    }

    @Test
    public void findTypesByNameExceptionTest() {
        assertThrows(InvalidRequestException.class, () -> typeService.findTypesByName(null));
        Mockito.when(repository.findByNameContains("Type")).thenReturn(new ArrayList<>());
        assertThrows(ResourceNotFoundException.class, () -> typeService.findTypesByName("Type"));
    }

    @Test
    public void findBooksByTypeIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(TYPE_1));
        List<BookDto> result = typeService.findBooksByTypeId(1L);
        assertEquals(2, result.size());
        assertTrue(result.contains(BOOK_DTO_1));
        assertTrue(result.contains(BOOK_DTO_2));
    }

    @Test
    public void findBooksByTypeIdExceptionTest() {
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(TYPE_2));
        assertThrows(ResourceNotFoundException.class, () -> typeService.findBooksByTypeId(2L));
    }

    @Test
    public void saveTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(TYPE_1));
        Mockito.when(repository.findByName("Type 3")).thenReturn(Optional.empty());
        Mockito.when(repository.saveAndFlush(TYPE_1)).thenReturn(TYPE_1);
        TypeDto type = new TypeDto(1L, "Type 3");
        TypeDto result = typeService.save(type);
        assertEquals(type, result);
    }

    @Test
    public void saveNewTest() {
        Mockito.when(repository.findByName("Type 1")).thenReturn(Optional.empty());
        Type type = new Type();
        type.setName("Type 1");
        Mockito.when(repository.saveAndFlush(type)).thenReturn(TYPE_1);
        TypeDto newType = new TypeDto(null, "Type 1");
        TypeDto result = typeService.save(newType);
        assertEquals(TYPE_DTO_1, result);
    }

    @Test
    public void saveExceptionTest() {
        Mockito.when(repository.findByName("Type 1")).thenReturn(Optional.of(TYPE_1));
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(TYPE_2));
        TypeDto type = new TypeDto(2L, "Type 1");
        assertThrows(InvalidRequestException.class, () -> typeService.save(type));
    }

    @Test
    public void removeTypeByIdTest() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(TYPE_1));
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(TYPE_2));
        typeService.removeTypeById(2L);
        assertThrows(InvalidRequestException.class, () -> typeService.removeTypeById(1L));
    }
}