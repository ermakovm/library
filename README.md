# Library

## Dependencies
1) spring-boot-starter-web, version 2.2.2.RELEASE
2) spring-boot-starter-data-jpa, version 2.2.2.RELEASE
3) spring-boot-starter-mail, version 2.2.2.RELEASE
4) spring-boot-starter-quartz, version 2.2.2.RELEASE
5) spring-boot-starter-cache, version 2.2.2.RELEASE
6) hibernate-jcache, version: 5.4.9.Final
7) ehcache, version: 3.8.1
8) postgresql, version 42.2.9
9) spring-boot-starter-test, version 2.2.2.RELEASE
10) junit-jupiter, version 5.5.2
11) junit-jupiter-migrationsupport, version 5.5.2'
## Running
1) Update file `/src/main/resources/application.properties`:
    1) Remove line `spring.profiles.active=dev`
    2) Update `spring.datasource.url`
    3) Update `spring.datasource.username`
    4) Update `spring.datasource.password`
    5) Update `spring.mail.host`
    6) Update `spring.mail.port`
    7) Update `spring.mail.username`
    8) Update `spring.mail.password`
    9) Update `spring.mail.properties.mail.smtp.auth`
    10) Update `spring.mail.properties.mail.smtp.starttls.enable`
    11) Update `spring.mail.protocol`   
2) Update file `/src/test/resources/application.properties` according to the example
```spring.profiles.active=test
   #Datasource
   spring.datasource.driver-class-name=org.postgresql.Driver
   spring.datasource.url=jdbc:postgresql://localhost:5432/library
   spring.datasource.username=library
   spring.datasource.password=library
   spring.datasource.platform=postgres
   spring.datasource.initialization-mode=never
   #Jpa
   spring.jpa.database=postgresql
   spring.jpa.show-sql=true
   spring.jpa.generate-ddl=false
   spring.jpa.hibernate.ddl-auto=none
   spring.jpa.open-in-view=false
   spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQL10Dialect
   #Logging
   logging.level.org.hibernate=info
   #Mail
   #Application config
   quartz.config.string=0 0/5 * ? * * *
   days.for.return=30
   #Cache config
   spring.jpa.properties.hibernate.cache.use_query_cache=false
   spring.jpa.properties.hibernate.cache.use_second_level_cache=false
   spring.cache.type=none
```
## Usage
1) Author
    * Fields:
        * id - long, generated by application.
        * firstName - string 1-100, not null.
        * middleName - string 1-100, can be null.
        * lastName - string 1-100, not null.
        * Object with `firstName+middleName+lastName` should be unique.
    * Queries:
        1) Method = GET, URL = `/api/authors`, Body = 
            ```
           {
                "firstName":"name_to_search",
                "middleName":"name_to_search",
                "lastName":"name_to_search"
           }
           ```
           When body empty return all authors, otherwise return with the specified name.
        2) Method = GET, URL = `/api/authors/{id}`.Return author by `id`.
        3) Method = GET, URL = `/api/authors/{id}/books`. Return books by author with `id`.
        4) Method = POST, URL = `/api/authors`, Body = 
            ```
            {
                "firstName":"name_to_add",
                "middleName":"name_to_add",
                "lastName":"name_to_add"
            }
            ```
           Add new author. 
        5) Method = PUT, URL = `/api/authors/{id}`, Body = 
           ```
           {
                "firstName":"name_to_update",
                "middleName":"name_to_update",
                "lastName":"name_to_update"
           }
           ```         
            Update author with `id`.
        6) Method = DELETE, URL = `/api/authors/{id}`. Delete author with `id`.
    
2) Book
    * Fields:
        * id - long, generated by application.
        * title - string 1-100, not null.
        * isbn - string, unique, can be null.
        * edition - int, can be null.
        * release - Year, not null.
        * type - Type, not null.
        * publisher - Publisher, not null.
        * authors - List of Author, can be null.
        * categories - List of Categories, can be null.
        * If type, publisher, authors and categories don't exists, they creates by application.
    * Queries:
        1) Method = GET, URL = `/api/books`, Body = 
            ```
           {
                "title":"title_to_search"
           }
           ```
           When body empty return all books, otherwise return with the specified title.
        2) Method = GET, URL = `/api/books/{id}`. Return book by `id`.
        3) Method = GET, URL = `/api/books/{id}/rents`. Return rents by book with `id`.
        4) Method = POST, URL = `/api/books`, Body = 
            ```
           {
                "title": "title_to_add",
                "isbn": "isbn_to_add",
                "edition": edition_to_add,
                "release": year_of_release,
                "type": {
                  	"name":"type_name"
                },
                "publisher": {
                   	"name":"publisher_name",
                    "address":"address_name"
                },
                "authors": [
                    {
                        "firstName":"author1_firstName",
                        "lastName":"author1_middleName"
                    },
                    {
                        "firstName":"author2_firstName",
                        "middleName":"author2_middleNAme",
                        "lastName":"author2_lastName"
                    }
                ],
                "categories": [
                    {
                        "name":"category1_name"
                    },
                    {
                        "name":"category2_name"
                    }
                ]
           }
           ```
           Add new book.
        5) Method = PUT, URL = `/api/books/{id}`, Body = 
            ```
           {
                "title": "title_to_update",
                "isbn": "isbn_to_update",
                "edition": edition_to_update,
                "release": year_of_release,
                "type": {
                  	"name":"type_name"
                },
                "publisher": {
                   	"name":"publisher_name",
                    "address":"address_name"
                },
                "authors": [
                    {
                        "firstName":"author1_firstName",
                        "lastName":"author1_middleName"
                    },
                    {
                        "firstName":"author2_firstName",
                        "middleName":"author2_middleNAme",
                        "lastName":"author2_lastName"
                    }
                ],
                "categories": [
                    {
                        "name":"category1_name"
                    },
                    {
                        "name":"category2_name"
                    }
                ]
           }
           ```
           Update book with `id`.
        6) Method = DELETE, URL = `/api/books/{id}`. Delete book with `id`.
        
3) Rent
    * Fields:
        * id - long, generated by application.
        * client_id - long, not null.
        * book_id - long, not null.
        * issueDate - LocalDate, not null (Format for input: `dd:MM:yyyy`).
        * returnDate - LocalDate, can be null (Format for input: `dd:MM:yyyy`).
    * Queries:
        1) Method = GET, URL = `/api/rents`. Return all rents.
        2) Method = GET, URL = `/api/rents/{id}`. Return rent by `id`.
        3) Method = GET, URL = `/api/rents/noreturn` Return rents where `returnDate = null`
        4) Method = GET, URL = `/api/rents/expired` Return expired rents.
        5) Method = POST, URL = `/api/rents`, Body = 
            ```
            {
                "client_id":client_id,
                "book_id:book_id,
                "issueDate":"dd:MM:yyyy",
                "returnDate":"dd:MM:yyyy"
            }
            ```
            Add new rent.
        6) Method = PUT, URL = `/api/rents/{id}`, Body =
            ```
            {
                "client_id":client_id,
                "book_id:book_id,
                "issueDate":"dd:MM:yyyy",
                "returnDate":"dd:MM:yyyy"
            }
            ``` 
           Update rent with `id`.
        7) Method = DELETE, URL = `/api/rents/{id}`. Delete rent with `id`.

4) Category (for example Fantasy, Horror etc)
    * Fields:
        * id - long, generated by application.
        * name - string 1-100, unique, not null.
    * Queries:
        1) Method = GET, URL = `/api/categories`, Body = 
           ```
           {
                "name":"name_to_search"
           }
           ```
           When body empty return all categories, otherwise return with the specified name.
        2) Method = GET, URL = `/api/categories/{id}`. Return category by `id`.
        3) Method = GET, URL = `/api/categories/{id}/books`. Return books of category with `id`.
        4) Method = POST, URL = `/api/categories`, Body =
           ```
           {
                "name":"name_to_add"
           }
           ``` 
            Add new category.
        5) Method = PUT, URL = `/api/categories/{id}`, Body = 
            ```
            {
                "name":"name_to_update"
            }
            ```
            Update category with `id`.
        6) Method = DELETE, URL = `/api/categories/{id}`. Delete category with `id`.

5) Client
    * Fields:
        * id - long, generated by application.
        * firstName - string 1-100, not null.
        * middleName - string 1-100, can be null.
        * lastName - string 1-100, not null.
        * email string 1-100, not null, unique.
        * phone string 1-100, not null, unique.
        * address string 1-500, not null.
    * Queries: 
        1) Method = GET, URL = `/api/clients`, Body = 
            ```
            {
                "firstName":"name_to_search",
                "middleName":"name_to_search",
                "lastName":"name_to_search"
            }
            ```
        
            When body empty return all clients, otherwise return with the specified name.
        2) Method = GET, URL = `/api/clients/email`, Body = `{"email":"email_to_search"}`. Find client with specified email.
        3) Method = GET, URL = `/api/clients/phone`, Body = `{"phone":"phone_to_search}`. Find client with specified phone.
        4) Method = GET, URL = `/api/clients/{id}`. Return client by `id`.
        5) Method = GET, URL = `/api/clients/{id}/rents`. Return rents by client with `id`.
        6) Method = POST, URL = `/api/clients`, Body = 
            ```
            {
                "firstName":"name_to_add",
                "middleName":"name_to_add",
                "lastName":"name_to_add",
                "email":"email_to_add",
                "phone":"phone_to_add",
                "address":"address_to_add"
            }
            ```
            Add new client.
        7) Method = PUT, URL = `/api/clients/{id}`, Body = 
            ```
            {
                "firstName":"name_to_update",
                "middleName":"name_to_update",
                "lastName":"name_to_update",
                "email":"email_to_update",
                "phone":"phone_to_update",
                "address":"address_to_update"
            } 
            ```
           Update client with `id`.
        8) Method = DELETE, URL = `/api/clients/{id}`. Delete client with `id`.
        
6) Publisher
    * Fields:
        * id - long, generated by application.
        * name - string 1-100, must be unique, not null.
        * address - string 1-500, not null
    * Queries:
        1) Method = GET, URL = `/api/publishers`, Body = 
           ```
           {
                "name":"name_for_search"
           }
           ```
           When body empty return all publishers, otherwise return with the specified name.
        2) Method = GET, URL = `/api/publishers/{id}`. Return publisher by `id`.
        3) Method = GET, URL = `/api/publishers/{id}/books`. Return books of publisher with `id`.
        4) Method = POST, URL = `/api/publishers`, Body = 
           ```
           {
                "name":"name_to_add",
                "address":"address_to_add"
           }
           ```
           Add new publisher
        5) Method = PUT, URL = `/api/publishers/{id}`, Body = 
            ```
            {
                "name":"name_to_update",
                "address":"address_to_update"
            }           
            ```
            Update publisher with `id`.
        6) Method = DELETE, URL = `/api/publishers/{id}` Delete publisher with `id`.
        
7) Type (for example: Books, Magazines, Newspapers etc)
    * Fields:
        * id - long, generated by application. 
        * name - string 1-100, must be unique, not null.
    * Queries:
        1) Method = GET, URL = `/api/types`, Body = 
            ```
            {
                "name":"name_to_search"
            }
            ``` 
            When body empty return all types, otherwise return with the specified name.
        2) Method = GET, URL = `/api/types/{id}`. Return type by `id`.
        3) Method = GET, URL = `/api/types/{id}/books`. Return books of type with `id`.
        4) Method = POST, URL = `/api/types`, Body = 
            ```
            {
                "name":"name_to_add"
            }
            ```
            Add new type.
        5) Method = PUT, URL = `/api/types/{id}`, Body = 
           ```
           {
                "name":"name_to_update"
           }
           ```
            Update type with `id`.
        6) Method = DELETE, URL = `/api/types/{id}`. Delete type with `id`.
